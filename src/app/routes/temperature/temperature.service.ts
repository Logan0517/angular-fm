import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class TemperatureService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'temperature/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'temperature/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'temperature/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'temperature/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'temperature/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'temperature/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'temperature/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'temperature/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'temperature/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'temperature/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'temperature/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'temperature/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'temperature/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'temperature/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'temperature/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'temperature/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'temperature/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'temperature/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'temperature/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'temperature/getForRelation';
        return this.http.post(url,vo);
    }
}
