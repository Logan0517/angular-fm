import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { TemperatureViewComponent } from '../view/temperature-view.component';
import { TemperatureEditComponent } from '../edit/temperature-edit.component';
import { TemperatureService } from '../temperature.service';
import { Temperature } from '../temperature';
import { TemperatureVO } from '../temperature.vo';

@Component({
    selector: 'app-temperature-list',
    templateUrl: './temperature-list.component.html',
    styles: [],
    providers: [TemperatureEditComponent,TemperatureViewComponent],
})
export class TemperatureListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private temperatureService: TemperatureService,
  ) {}

  temperature: Temperature;
  temperatureVO: TemperatureVO;
  temperatureVOs: TemperatureVO[];
  temperatures: Temperature[];
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '数据标识', index: 'id' },
    { title: '探头排序号码', index: 'probeSortNum' },
    { title: '编码', index: 'code' },
    { title: '设备编码', index: 'deviceCode' },
    { title: '温度数据', index: 'temperature' },
    { title: '报警温度镜像', index: 'warnningTemperature' },
    { title: '', index: 'isDel' },
    { title: '上传时间', index: 'time' },
    { title: '是否报警', index: 'isWarnning' },
    { title: '分组排序号码', index: 'groupSortNum' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Temperature) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Temperature) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Temperature) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.temperatureVO = new TemperatureVO();
    this.temperatureVO.page = 1;
    this.temperatureVO.pageSize = 15;
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
       timeEndBegin: new FormControl({ name: 'timeEndBegin', value: '' }),/*上传时间*/
    });
  }

  search() {
    this.loading = true;
    this.temperatureService
      .getByPage(this.temperatureVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.temperatures = res.datas;
        this.temperatureVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.temperatureVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Temperature) {
    this.modal.createStatic(TemperatureEditComponent, { temperatureVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Temperature) {
    this.loading = true;
    this.temperatureService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Temperature) {
    this.router.navigate(['/temperature/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.temperatureVOs = [];
    this.selectedRows.forEach(selectRow => {
      const temperatureTmpVO: TemperatureVO = new TemperatureVO();
      temperatureTmpVO.id = selectRow.id;
      this.temperatureVOs.push(temperatureTmpVO);
    });
    this.temperatureService
    .delByVOForLogicMul(this.temperatureVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.temperatureVOs = [];
    this.selectedRows.forEach(selectRow => {
    const temperatureTmpVO: TemperatureVO = new TemperatureVO();
      temperatureTmpVO.id = selectRow.id;
    this.temperatureVOs.push(temperatureTmpVO);
    });
    this.temperatureService
    .updMul(this.temperatureVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
