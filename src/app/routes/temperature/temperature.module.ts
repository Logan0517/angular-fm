import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { TemperatureRoutingModule } from './temperature-routing.module';
import { TemperatureListComponent } from './list/temperature-list.component';
import { TemperatureViewComponent } from './view/temperature-view.component';
import { TemperatureEditComponent } from './edit/temperature-edit.component';

const COMPONENTS = [TemperatureListComponent,TemperatureViewComponent];
const COMPONENTS_NOROUNT = [TemperatureEditComponent];

@NgModule({
    imports: [SharedModule,TemperatureRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class TemperatureModule { }
