import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { TemperatureService } from '../temperature.service';
import { Temperature } from '../temperature';
import { TemperatureVO } from '../temperature.vo';

@Component({
  selector: 'app-temperature-edit',
  templateUrl: './temperature-edit.component.html',
})
export class TemperatureEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private temperatureService: TemperatureService,
  ) {}

  form: FormGroup;
  loading = false;
  temperature: Temperature = new Temperature();
  temperatureVO: TemperatureVO = new TemperatureVO();

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.temperature = new Temperature();
    if(!this.temperatureVO){
        this.temperatureVO = new TemperatureVO();
    }
    if(this.temperatureVO.id){
      this.loading = false;
      this.temperatureService
        .getByVo(this.temperatureVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.temperature = res;
        });
    }
  }

  initForm() {
    this.form = new FormGroup({
        probeSortNum: new FormControl({ name: 'probeSortNum', value: '' }, [Validators.required]),/*探头排序号码*/
        code: new FormControl({ name: 'code', value: '' }, [Validators.required]),/*编码*/
        deviceCode: new FormControl({ name: 'deviceCode', value: '' }, [Validators.required]),/*设备编码*/
        temperature: new FormControl({ name: 'temperature', value: '' }, [Validators.required]),/*温度数据*/
        warnningTemperature: new FormControl({ name: 'warnningTemperature', value: '' }, [Validators.required]),/*报警温度镜像*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        time: new FormControl({ name: 'time', value: '' }, [Validators.required]),/*上传时间*/
        isWarnning: new FormControl({ name: 'isWarnning', value: '' }, [Validators.required]),/*是否报警*/
        groupSortNum: new FormControl({ name: 'groupSortNum', value: '' }, [Validators.required]),/*分组排序号码*/
    });
  }
    submit() {
        if (this.temperature.id) {
            this.temperatureService
                .upd(this.temperature)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.temperatureService
            .add(this.temperature)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
