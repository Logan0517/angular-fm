import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { TemperatureService } from '../temperature.service';
import { Temperature } from '../temperature';
import { TemperatureVO } from '../temperature.vo';


@Component({
    selector: 'app-temperature-view',
    templateUrl: './temperature-view.component.html',
})
export class TemperatureViewComponent implements OnInit {

    loading = false;
    temperature: Temperature = new Temperature();
    temperatureVO: TemperatureVO = new TemperatureVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private temperatureService:TemperatureService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Temperature) => {
            this.temperatureVO.id = param.id;
        });
        this.loading = true;
        this.temperatureService
        .getByVo(this.temperatureVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Temperature) => {
            this.temperature = res;
        });
    }


}
