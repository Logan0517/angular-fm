import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemperatureListComponent } from './list/temperature-list.component';
import { TemperatureViewComponent } from './view/temperature-view.component';

const routes: Routes = [
    { path: 'list', component: TemperatureListComponent },
    { path: 'view/:id', component: TemperatureViewComponent, data: { title: '温度数据'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TemperatureRoutingModule { }
