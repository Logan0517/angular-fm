import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProbeListComponent } from './list/probe-list.component';
import { ProbeViewComponent } from './view/probe-view.component';

const routes: Routes = [
    { path: 'list', component: ProbeListComponent },
    { path: 'view/:id', component: ProbeViewComponent, data: { title: '探头'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProbeRoutingModule { }
