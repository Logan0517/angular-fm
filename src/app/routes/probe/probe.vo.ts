import { ProbeGroupVO } from '../probeGroup/probeGroup.vo';
import { DeviceVO } from '../device/device.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Probe 探头
*/
export class ProbeVO {
    id: any;/**/
    code: any;/*编码*/
    groupCode: any;/*分组*/
    groupNum: any;/*分组序号 0代表没有分组*/
    deviceCode: any;/*设备编码*/
    sortNum: any;/*排序编号 从1开始*/
    name: any;/*名称*/
    nameLike: any;/*名称模糊查询*/
    isDel: any;/**/
    deviceBusCode: any;/*设备业务编码*/
    groupVO: ProbeGroupVO;/*分组*/
    deviceVO: DeviceVO;/*设备编码*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}