import { ProbeGroup } from '../probeGroup/probeGroup';
import { Device } from '../device/device';

/*
* Created by jiangchen
* Automated Build
* 实体 Probe 探头
*/
export class Probe {
    id: any;/**/
    code: any;/*编码*/
    groupCode: any;/*分组*/
    groupNum: any;/*分组序号 0代表没有分组*/
    deviceCode: any;/*设备编码*/
    sortNum: any;/*排序编号 从1开始*/
    name: any;/*名称*/
    isDel: any;/**/
    deviceBusCode: any;/*设备业务编码*/
    group: ProbeGroup;/*分组*/
    device: Device;/*设备编码*/
}