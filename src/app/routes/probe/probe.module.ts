import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ProbeRoutingModule } from './probe-routing.module';
import { ProbeListComponent } from './list/probe-list.component';
import { ProbeViewComponent } from './view/probe-view.component';
import { ProbeEditComponent } from './edit/probe-edit.component';

const COMPONENTS = [ProbeListComponent,ProbeViewComponent];
const COMPONENTS_NOROUNT = [ProbeEditComponent];

@NgModule({
    imports: [SharedModule,ProbeRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class ProbeModule { }
