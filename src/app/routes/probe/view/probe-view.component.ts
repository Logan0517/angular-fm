import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { ProbeService } from '../probe.service';
import { Probe } from '../probe';
import { ProbeVO } from '../probe.vo';
import { ProbeGroupService } from '../../probeGroup/probeGroup.service';
import { ProbeGroup } from '../../probeGroup/probeGroup';
import { ProbeGroupVO } from '../../probeGroup/probeGroup.vo';
import { DeviceService } from '../../device/device.service';
import { Device } from '../../device/device';
import { DeviceVO } from '../../device/device.vo';


@Component({
    selector: 'app-probe-view',
    templateUrl: './probe-view.component.html',
})
export class ProbeViewComponent implements OnInit {

    loading = false;
    probe: Probe = new Probe();
    probeVO: ProbeVO = new ProbeVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private probeService:ProbeService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Probe) => {
            this.probeVO.id = param.id;
        });
        this.loading = true;
        this.probeService
        .getByVo(this.probeVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Probe) => {
            this.probe = res;
        });
    }


}
