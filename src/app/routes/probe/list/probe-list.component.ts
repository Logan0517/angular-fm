import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { ProbeViewComponent } from '../view/probe-view.component';
import { ProbeEditComponent } from '../edit/probe-edit.component';
import { ProbeService } from '../probe.service';
import { Probe } from '../probe';
import { ProbeVO } from '../probe.vo';
import { ProbeGroupService } from '../../probeGroup/probeGroup.service';
import { ProbeGroup } from '../../probeGroup/probeGroup';
import { ProbeGroupVO } from '../../probeGroup/probeGroup.vo';
import { DeviceService } from '../../device/device.service';
import { Device } from '../../device/device';
import { DeviceVO } from '../../device/device.vo';

@Component({
    selector: 'app-probe-list',
    templateUrl: './probe-list.component.html',
    styles: [],
    providers: [ProbeEditComponent,ProbeViewComponent],
})
export class ProbeListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private probeService: ProbeService,
    private probeGroupService: ProbeGroupService,
    private deviceService: DeviceService,
  ) {}

  probe: Probe;
  probeVO: ProbeVO;
  probeVOs: ProbeVO[];
  probes: Probe[];
  groups: ProbeGroup[];
  groupVO: ProbeGroupVO;
  devices: Device[];
  deviceVO: DeviceVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '编码', index: 'code' },
    { title: '分组', index: 'groupCode' },
    { title: '分组序号 0代表没有分组', index: 'groupNum' },
    { title: '设备编码', index: 'deviceCode' },
    { title: '排序编号 从1开始', index: 'sortNum' },
    { title: '名称', index: 'name' },
    { title: '', index: 'isDel' },
    { title: '设备业务编码', index: 'deviceBusCode' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Probe) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Probe) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Probe) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.probeVO = new ProbeVO();
    this.probeVO.page = 1;
    this.probeVO.pageSize = 15;
    this.groupVO = new ProbeGroupVO();
    this.probeGroupService.getAllByVO(this.groupVO).subscribe((res: ProbeGroup[]) => {
      this.groups = res;
    });
    this.deviceVO = new DeviceVO();
    this.deviceService.getAllByVO(this.deviceVO).subscribe((res: Device[]) => {
      this.devices = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*名称模糊查询*/
      group: new FormControl({ name: 'group', value: '' }),/*分组*/
      device: new FormControl({ name: 'device', value: '' }),/*设备编码*/
    });
  }

  search() {
    this.loading = true;
    this.probeService
      .getByPage(this.probeVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.probes = res.datas;
        this.probeVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.probeVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Probe) {
    this.modal.createStatic(ProbeEditComponent, { probeVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Probe) {
    this.loading = true;
    this.probeService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Probe) {
    this.router.navigate(['/probe/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.probeVOs = [];
    this.selectedRows.forEach(selectRow => {
      const probeTmpVO: ProbeVO = new ProbeVO();
      probeTmpVO.id = selectRow.id;
      this.probeVOs.push(probeTmpVO);
    });
    this.probeService
    .delByVOForLogicMul(this.probeVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.probeVOs = [];
    this.selectedRows.forEach(selectRow => {
    const probeTmpVO: ProbeVO = new ProbeVO();
      probeTmpVO.id = selectRow.id;
    this.probeVOs.push(probeTmpVO);
    });
    this.probeService
    .updMul(this.probeVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
