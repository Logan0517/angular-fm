import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { ProbeService } from '../probe.service';
import { Probe } from '../probe';
import { ProbeVO } from '../probe.vo';
import { ProbeGroupService } from '../../probeGroup/probeGroup.service';
import { ProbeGroup } from '../../probeGroup/probeGroup';
import { ProbeGroupVO } from '../../probeGroup/probeGroup.vo';
import { DeviceService } from '../../device/device.service';
import { Device } from '../../device/device';
import { DeviceVO } from '../../device/device.vo';

@Component({
  selector: 'app-probe-edit',
  templateUrl: './probe-edit.component.html',
})
export class ProbeEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private probeService: ProbeService,
    private probeGroupService: ProbeGroupService,
    private deviceService: DeviceService,
  ) {}

  form: FormGroup;
  loading = false;
  probe: Probe = new Probe();
  probeVO: ProbeVO = new ProbeVO();
  groups: ProbeGroup[];
  groupVO: ProbeGroupVO;
  devices: Device[];
  deviceVO: DeviceVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.probe = new Probe();
    if(!this.probeVO){
        this.probeVO = new ProbeVO();
    }
    if(this.probeVO.id){
      this.loading = false;
      this.probeService
        .getByVo(this.probeVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.probe = res;
        });
    }
    this.groupVO = new ProbeGroupVO();
    this.probeGroupService.getAllByVO(this.groupVO).subscribe((res: ProbeGroup[]) => {
      this.groups = res;
    });
    this.deviceVO = new DeviceVO();
    this.deviceService.getAllByVO(this.deviceVO).subscribe((res: Device[]) => {
      this.devices = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        group: new FormControl({ name: 'group', value: '' }, Validators.required),/*分组*/
        device: new FormControl({ name: 'device', value: '' }, Validators.required),/*设备编码*/
        code: new FormControl({ name: 'code', value: '' }, [Validators.required]),/*编码*/
        groupCode: new FormControl({ name: 'groupCode', value: '' }, [Validators.required]),/*分组*/
        groupNum: new FormControl({ name: 'groupNum', value: '' }, [Validators.required]),/*分组序号 0代表没有分组*/
        deviceCode: new FormControl({ name: 'deviceCode', value: '' }, [Validators.required]),/*设备编码*/
        sortNum: new FormControl({ name: 'sortNum', value: '' }, [Validators.required]),/*排序编号 从1开始*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        deviceBusCode: new FormControl({ name: 'deviceBusCode', value: '' }, [Validators.required]),/*设备业务编码*/
    });
  }
    submit() {
        if (this.probe.id) {
            this.probeService
                .upd(this.probe)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.probeService
            .add(this.probe)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
