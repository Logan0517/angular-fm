import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class ProbeService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'probe/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'probe/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'probe/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'probe/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'probe/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'probe/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'probe/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'probe/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'probe/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'probe/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'probe/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'probe/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'probe/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'probe/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'probe/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'probe/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'probe/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'probe/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'probe/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'probe/getForRelation';
        return this.http.post(url,vo);
    }
}
