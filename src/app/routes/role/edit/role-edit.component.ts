import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { RoleService } from '../role.service';
import { Role } from '../role';
import { RoleVO } from '../role.vo';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
})
export class RoleEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private roleService: RoleService,
  ) {}

  form: FormGroup;
  loading = false;
  role: Role = new Role();
  roleVO: RoleVO = new RoleVO();

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.role = new Role();
    if(!this.roleVO){
        this.roleVO = new RoleVO();
    }
    if(this.roleVO.id){
      this.loading = false;
      this.roleService
        .getByVo(this.roleVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.role = res;
        });
    }
  }

  initForm() {
    this.form = new FormGroup({
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*角色名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/*逻辑删除*/
    });
  }
    submit() {
        if (this.role.id) {
            this.roleService
                .upd(this.role)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.roleService
            .add(this.role)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
