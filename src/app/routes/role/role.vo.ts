import { GroupVO } from '../group/group.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Role 角色
*/
export class RoleVO {
    id: any;/**/
    name: any;/*角色名称*/
    nameLike: any;/*角色名称模糊查询*/
    isDel: any;/*逻辑删除*/
    roleGroupsVO: GroupVO[];/* 用户分组*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}