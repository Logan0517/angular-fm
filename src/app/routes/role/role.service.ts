import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class RoleService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'role/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'role/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'role/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'role/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'role/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'role/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'role/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'role/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'role/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'role/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'role/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'role/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'role/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'role/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'role/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'role/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'role/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'role/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'role/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'role/getForRelation';
        return this.http.post(url,vo);
    }
}
