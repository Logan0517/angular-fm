import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleListComponent } from './list/role-list.component';
import { RoleViewComponent } from './view/role-view.component';

const routes: Routes = [
    { path: 'list', component: RoleListComponent },
    { path: 'view/:id', component: RoleViewComponent, data: { title: '角色'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoleRoutingModule { }
