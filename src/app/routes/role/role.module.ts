import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { RoleRoutingModule } from './role-routing.module';
import { RoleListComponent } from './list/role-list.component';
import { RoleViewComponent } from './view/role-view.component';
import { RoleEditComponent } from './edit/role-edit.component';

const COMPONENTS = [RoleListComponent,RoleViewComponent];
const COMPONENTS_NOROUNT = [RoleEditComponent];

@NgModule({
    imports: [SharedModule,RoleRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class RoleModule { }
