import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { RoleService } from '../role.service';
import { Role } from '../role';
import { RoleVO } from '../role.vo';

import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';

@Component({
    selector: 'app-role-view',
    templateUrl: './role-view.component.html',
})
export class RoleViewComponent implements OnInit {

    loading = false;
    role: Role = new Role();
    roleVO: RoleVO = new RoleVO();
    /** 用户分组 */
    roleGroups: Group[] = [];
    roleGroupsVO: GroupVO = new GroupVO();
    roleGroupsLoading = false;

    /** 用户分组 */
    roleGroupsColumns: STColumn[] = [
        { title: '组名', index: 'name', default: '-'},
        { title: '逻辑删除', index: 'isDel', default: '-'},
        { title: '标识', index: 'id', default: '-'},
        { title: '角色', index: 'roleId', default: '-'},
        { title: '职位', index: 'positionId', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private roleService:RoleService,
        private groupService:GroupService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initRoleGroups();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Role) => {
            this.roleVO.id = param.id;
        });
        this.loading = true;
        this.roleService
        .getByVo(this.roleVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Role) => {
            this.role = res;
        });
    }

    /** 用户分组 */
    initRoleGroups() {
        this.roleGroupsLoading = true;
        this.roleGroupsVO.roleVO= this.roleVO;
        this.groupService
        .getAllByVO(this.roleGroupsVO)
        .pipe(finalize(() => (this.roleGroupsLoading = false)))
        .subscribe((res: Group[]) => {
            this.roleGroups = res;
        });
    }


}
