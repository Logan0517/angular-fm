import { Group } from '../group/group';

/*
* Created by jiangchen
* Automated Build
* 实体 Role 角色
*/
export class Role {
    id: any;/**/
    name: any;/*角色名称*/
    isDel: any;/*逻辑删除*/
    roleGroups: Group[];/* 用户分组*/
}