import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { RoleViewComponent } from '../view/role-view.component';
import { RoleEditComponent } from '../edit/role-edit.component';
import { RoleService } from '../role.service';
import { Role } from '../role';
import { RoleVO } from '../role.vo';

@Component({
    selector: 'app-role-list',
    templateUrl: './role-list.component.html',
    styles: [],
    providers: [RoleEditComponent,RoleViewComponent],
})
export class RoleListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private roleService: RoleService,
  ) {}

  role: Role;
  roleVO: RoleVO;
  roleVOs: RoleVO[];
  roles: Role[];
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '角色名称', index: 'name' },
    { title: '逻辑删除', index: 'isDel' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Role) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Role) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Role) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.roleVO = new RoleVO();
    this.roleVO.page = 1;
    this.roleVO.pageSize = 15;
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*角色名称模糊查询*/
    });
  }

  search() {
    this.loading = true;
    this.roleService
      .getByPage(this.roleVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.roles = res.datas;
        this.roleVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.roleVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Role) {
    this.modal.createStatic(RoleEditComponent, { roleVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Role) {
    this.loading = true;
    this.roleService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Role) {
    this.router.navigate(['/role/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.roleVOs = [];
    this.selectedRows.forEach(selectRow => {
      const roleTmpVO: RoleVO = new RoleVO();
      roleTmpVO.id = selectRow.id;
      this.roleVOs.push(roleTmpVO);
    });
    this.roleService
    .delByVOForLogicMul(this.roleVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.roleVOs = [];
    this.selectedRows.forEach(selectRow => {
    const roleTmpVO: RoleVO = new RoleVO();
      roleTmpVO.id = selectRow.id;
    this.roleVOs.push(roleTmpVO);
    });
    this.roleService
    .updMul(this.roleVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
