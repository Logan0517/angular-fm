import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WarnningDataListComponent } from './list/warnningData-list.component';
import { WarnningDataViewComponent } from './view/warnningData-view.component';

const routes: Routes = [
    { path: 'list', component: WarnningDataListComponent },
    { path: 'view/:id', component: WarnningDataViewComponent, data: { title: '报警数据'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WarnningDataRoutingModule { }
