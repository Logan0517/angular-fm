
/*
* Created by jiangchen
* Automated Build
* 实体 WarnningData 报警数据
*/
export class WarnningDataVO {
    id: any;/**/
    probeSortNum: any;/*探头排序号码*/
    code: any;/*编码*/
    deviceCode: any;/*设备业务编码*/
    temperature: any;/*温度数据*/
    isDel: any;/**/
    temperatureCode: any;/*温度数据编码*/
    time: any;/*上传时间*/
    timeEnd: any;/*上传时间开始*/
    timeBegin: any;/*上传时间结束*/
    isWarnning: any;/*是否报警*/
    groupSortNum: any;/*分组排序号码*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}