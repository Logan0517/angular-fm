import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { WarnningDataService } from '../warnningData.service';
import { WarnningData } from '../warnningData';
import { WarnningDataVO } from '../warnningData.vo';

@Component({
  selector: 'app-warnningdata-edit',
  templateUrl: './warnningdata-edit.component.html',
})
export class WarnningDataEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private warnningDataService: WarnningDataService,
  ) {}

  form: FormGroup;
  loading = false;
  warnningData: WarnningData = new WarnningData();
  warnningDataVO: WarnningDataVO = new WarnningDataVO();

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.warnningData = new WarnningData();
    if(!this.warnningDataVO){
        this.warnningDataVO = new WarnningDataVO();
    }
    if(this.warnningDataVO.id){
      this.loading = false;
      this.warnningDataService
        .getByVo(this.warnningDataVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.warnningData = res;
        });
    }
  }

  initForm() {
    this.form = new FormGroup({
        probeSortNum: new FormControl({ name: 'probeSortNum', value: '' }, [Validators.required]),/*探头排序号码*/
        code: new FormControl({ name: 'code', value: '' }, [Validators.required]),/*编码*/
        deviceCode: new FormControl({ name: 'deviceCode', value: '' }, [Validators.required]),/*设备业务编码*/
        temperature: new FormControl({ name: 'temperature', value: '' }, [Validators.required]),/*温度数据*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        temperatureCode: new FormControl({ name: 'temperatureCode', value: '' }, [Validators.required]),/*温度数据编码*/
        time: new FormControl({ name: 'time', value: '' }, [Validators.required]),/*上传时间*/
        isWarnning: new FormControl({ name: 'isWarnning', value: '' }, [Validators.required]),/*是否报警*/
        groupSortNum: new FormControl({ name: 'groupSortNum', value: '' }, [Validators.required]),/*分组排序号码*/
    });
  }
    submit() {
        if (this.warnningData.id) {
            this.warnningDataService
                .upd(this.warnningData)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.warnningDataService
            .add(this.warnningData)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
