import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { WarnningDataRoutingModule } from './warnningData-routing.module';
import { WarnningDataListComponent } from './list/warnningData-list.component';
import { WarnningDataViewComponent } from './view/warnningData-view.component';
import { WarnningDataEditComponent } from './edit/warnningData-edit.component';

const COMPONENTS = [WarnningDataListComponent,WarnningDataViewComponent];
const COMPONENTS_NOROUNT = [WarnningDataEditComponent];

@NgModule({
    imports: [SharedModule,WarnningDataRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class WarnningDataModule { }
