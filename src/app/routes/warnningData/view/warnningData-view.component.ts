import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { WarnningDataService } from '../warnningData.service';
import { WarnningData } from '../warnningData';
import { WarnningDataVO } from '../warnningData.vo';


@Component({
    selector: 'app-warnningdata-view',
    templateUrl: './warnningdata-view.component.html',
})
export class WarnningDataViewComponent implements OnInit {

    loading = false;
    warnningData: WarnningData = new WarnningData();
    warnningDataVO: WarnningDataVO = new WarnningDataVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private warnningDataService:WarnningDataService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: WarnningData) => {
            this.warnningDataVO.id = param.id;
        });
        this.loading = true;
        this.warnningDataService
        .getByVo(this.warnningDataVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: WarnningData) => {
            this.warnningData = res;
        });
    }


}
