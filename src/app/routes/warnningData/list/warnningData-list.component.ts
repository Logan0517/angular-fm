import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { WarnningDataViewComponent } from '../view/warnningData-view.component';
import { WarnningDataEditComponent } from '../edit/warnningData-edit.component';
import { WarnningDataService } from '../warnningData.service';
import { WarnningData } from '../warnningData';
import { WarnningDataVO } from '../warnningData.vo';

@Component({
    selector: 'app-warnningdata-list',
    templateUrl: './warnningdata-list.component.html',
    styles: [],
    providers: [WarnningDataEditComponent,WarnningDataViewComponent],
})
export class WarnningDataListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private warnningDataService: WarnningDataService,
  ) {}

  warnningData: WarnningData;
  warnningDataVO: WarnningDataVO;
  warnningDataVOs: WarnningDataVO[];
  warnningDatas: WarnningData[];
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '探头排序号码', index: 'probeSortNum' },
    { title: '编码', index: 'code' },
    { title: '设备业务编码', index: 'deviceCode' },
    { title: '温度数据', index: 'temperature' },
    { title: '', index: 'isDel' },
    { title: '温度数据编码', index: 'temperatureCode' },
    { title: '上传时间', index: 'time' },
    { title: '是否报警', index: 'isWarnning' },
    { title: '分组排序号码', index: 'groupSortNum' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: WarnningData) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: WarnningData) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: WarnningData) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.warnningDataVO = new WarnningDataVO();
    this.warnningDataVO.page = 1;
    this.warnningDataVO.pageSize = 15;
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
       timeEndBegin: new FormControl({ name: 'timeEndBegin', value: '' }),/*上传时间*/
    });
  }

  search() {
    this.loading = true;
    this.warnningDataService
      .getByPage(this.warnningDataVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.warnningDatas = res.datas;
        this.warnningDataVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.warnningDataVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: WarnningData) {
    this.modal.createStatic(WarnningDataEditComponent, { warnningDataVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: WarnningData) {
    this.loading = true;
    this.warnningDataService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: WarnningData) {
    this.router.navigate(['/warnningData/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.warnningDataVOs = [];
    this.selectedRows.forEach(selectRow => {
      const warnningDataTmpVO: WarnningDataVO = new WarnningDataVO();
      warnningDataTmpVO.id = selectRow.id;
      this.warnningDataVOs.push(warnningDataTmpVO);
    });
    this.warnningDataService
    .delByVOForLogicMul(this.warnningDataVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.warnningDataVOs = [];
    this.selectedRows.forEach(selectRow => {
    const warnningDataTmpVO: WarnningDataVO = new WarnningDataVO();
      warnningDataTmpVO.id = selectRow.id;
    this.warnningDataVOs.push(warnningDataTmpVO);
    });
    this.warnningDataService
    .updMul(this.warnningDataVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
