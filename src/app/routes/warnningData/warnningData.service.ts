import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class WarnningDataService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'warnningData/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'warnningData/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'warnningData/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'warnningData/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'warnningData/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'warnningData/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'warnningData/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'warnningData/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'warnningData/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'warnningData/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'warnningData/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'warnningData/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'warnningData/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'warnningData/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'warnningData/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'warnningData/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'warnningData/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'warnningData/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'warnningData/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'warnningData/getForRelation';
        return this.http.post(url,vo);
    }
}
