import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PowerListComponent } from './list/power-list.component';
import { PowerViewComponent } from './view/power-view.component';

const routes: Routes = [
    { path: 'list', component: PowerListComponent },
    { path: 'view/:id', component: PowerViewComponent, data: { title: '权限'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PowerRoutingModule { }
