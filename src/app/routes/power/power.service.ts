import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class PowerService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'power/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'power/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'power/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'power/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'power/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'power/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'power/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'power/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'power/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'power/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'power/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'power/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'power/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'power/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'power/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'power/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'power/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'power/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'power/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'power/getForRelation';
        return this.http.post(url,vo);
    }
}
