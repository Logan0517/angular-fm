import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { PowerViewComponent } from '../view/power-view.component';
import { PowerEditComponent } from '../edit/power-edit.component';
import { PowerService } from '../power.service';
import { Power } from '../power';
import { PowerVO } from '../power.vo';

@Component({
    selector: 'app-power-list',
    templateUrl: './power-list.component.html',
    styles: [],
    providers: [PowerEditComponent,PowerViewComponent],
})
export class PowerListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private powerService: PowerService,
  ) {}

  power: Power;
  powerVO: PowerVO;
  powerVOs: PowerVO[];
  powers: Power[];
  parents: Power[];
  parentVO: PowerVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '父节点', index: 'parentId' },
    { title: '名称', index: 'name' },
    { title: '逻辑删除', index: 'isDel' },
    { title: '类型 1 展示类  2 操作类', index: 'type' },
    { title: '访问地址', index: 'url' },
    { title: '访问key', index: 'key' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Power) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Power) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Power) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.powerVO = new PowerVO();
    this.powerVO.page = 1;
    this.powerVO.pageSize = 15;
    this.parentVO = new PowerVO();
    this.powerService.getAllByVO(this.parentVO).subscribe((res: Power[]) => {
      this.parents = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*名称模糊查询*/
      parent: new FormControl({ name: 'parent', value: '' }),/*父节点*/
    });
  }

  search() {
    this.loading = true;
    this.powerService
      .getByPage(this.powerVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.powers = res.datas;
        this.powerVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.powerVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Power) {
    this.modal.createStatic(PowerEditComponent, { powerVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Power) {
    this.loading = true;
    this.powerService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Power) {
    this.router.navigate(['/power/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.powerVOs = [];
    this.selectedRows.forEach(selectRow => {
      const powerTmpVO: PowerVO = new PowerVO();
      powerTmpVO.id = selectRow.id;
      this.powerVOs.push(powerTmpVO);
    });
    this.powerService
    .delByVOForLogicMul(this.powerVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.powerVOs = [];
    this.selectedRows.forEach(selectRow => {
    const powerTmpVO: PowerVO = new PowerVO();
      powerTmpVO.id = selectRow.id;
    this.powerVOs.push(powerTmpVO);
    });
    this.powerService
    .updMul(this.powerVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
