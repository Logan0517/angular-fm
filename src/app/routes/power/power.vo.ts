import { PowerHasGroupVO } from '../powerHasGroup/powerHasGroup.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Power 权限
*/
export class PowerVO {
    id: any;/**/
    parentId: any;/*父节点*/
    name: any;/*名称*/
    nameLike: any;/*名称模糊查询*/
    isDel: any;/*逻辑删除*/
    type: any;/*类型 1 展示类  2 操作类*/
    url: any;/*访问地址*/
    key: any;/*访问key*/
    parentVO: PowerVO;/*父节点*/
    powerPowerHasGroupsVO: PowerHasGroupVO[];/* 权限分组多对多*/
    parentPowersVO: PowerVO[];/* 权限*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}