import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { PowerRoutingModule } from './power-routing.module';
import { PowerListComponent } from './list/power-list.component';
import { PowerViewComponent } from './view/power-view.component';
import { PowerEditComponent } from './edit/power-edit.component';

const COMPONENTS = [PowerListComponent,PowerViewComponent];
const COMPONENTS_NOROUNT = [PowerEditComponent];

@NgModule({
    imports: [SharedModule,PowerRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class PowerModule { }
