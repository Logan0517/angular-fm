import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { PowerService } from '../power.service';
import { Power } from '../power';
import { PowerVO } from '../power.vo';

@Component({
  selector: 'app-power-edit',
  templateUrl: './power-edit.component.html',
})
export class PowerEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private powerService: PowerService,
  ) {}

  form: FormGroup;
  loading = false;
  power: Power = new Power();
  powerVO: PowerVO = new PowerVO();
  parents: Power[];
  parentVO: PowerVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.power = new Power();
    if(!this.powerVO){
        this.powerVO = new PowerVO();
    }
    if(this.powerVO.id){
      this.loading = false;
      this.powerService
        .getByVo(this.powerVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.power = res;
        });
    }
    this.parentVO = new PowerVO();
    this.powerService.getAllByVO(this.parentVO).subscribe((res: Power[]) => {
      this.parents = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        parent: new FormControl({ name: 'parent', value: '' }, Validators.required),/*父节点*/
        parentId: new FormControl({ name: 'parentId', value: '' }, [Validators.required]),/*父节点*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/*逻辑删除*/
        type: new FormControl({ name: 'type', value: '' }, [Validators.required]),/*类型 1 展示类  2 操作类*/
        url: new FormControl({ name: 'url', value: '' }, [Validators.required]),/*访问地址*/
        key: new FormControl({ name: 'key', value: '' }, [Validators.required]),/*访问key*/
    });
  }
    submit() {
        if (this.power.id) {
            this.powerService
                .upd(this.power)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.powerService
            .add(this.power)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
