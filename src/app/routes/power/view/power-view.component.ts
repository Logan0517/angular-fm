import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { PowerService } from '../power.service';
import { Power } from '../power';
import { PowerVO } from '../power.vo';

import { PowerHasGroupService } from '../../powerHasGroup/powerHasGroup.service';
import { PowerHasGroup } from '../../powerHasGroup/powerHasGroup';
import { PowerHasGroupVO } from '../../powerHasGroup/powerHasGroup.vo';

@Component({
    selector: 'app-power-view',
    templateUrl: './power-view.component.html',
})
export class PowerViewComponent implements OnInit {

    loading = false;
    power: Power = new Power();
    powerVO: PowerVO = new PowerVO();
    /** 权限分组多对多 */
    powerPowerHasGroups: PowerHasGroup[] = [];
    powerPowerHasGroupsVO: PowerHasGroupVO = new PowerHasGroupVO();
    powerPowerHasGroupsLoading = false;
    /** 权限 */
    parentPowers: Power[] = [];
    parentPowersVO: PowerVO = new PowerVO();
    parentPowersLoading = false;

    /** 权限分组多对多 */
    powerPowerHasGroupsColumns: STColumn[] = [
        { title: '逻辑删除', index: 'isDel', default: '-'},
        { title: '分组', index: 'powerId', default: '-'},
        { title: '用户', index: 'groupId', default: '-'},
    ];
    /** 权限 */
    parentPowersColumns: STColumn[] = [
        { title: '父节点', index: 'parentId', default: '-'},
        { title: '名称', index: 'name', default: '-'},
        { title: '逻辑删除', index: 'isDel', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '类型 1 展示类  2 操作类', index: 'type', default: '-'},
        { title: '访问地址', index: 'url', default: '-'},
        { title: '访问key', index: 'key', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private powerService:PowerService,
        private powerHasGroupService:PowerHasGroupService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initPowerPowerHasGroups();
        this.initParentPowers();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Power) => {
            this.powerVO.id = param.id;
        });
        this.loading = true;
        this.powerService
        .getByVo(this.powerVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Power) => {
            this.power = res;
        });
    }

    /** 权限分组多对多 */
    initPowerPowerHasGroups() {
        this.powerPowerHasGroupsLoading = true;
        this.powerPowerHasGroupsVO.powerVO= this.powerVO;
        this.powerHasGroupService
        .getAllByVO(this.powerPowerHasGroupsVO)
        .pipe(finalize(() => (this.powerPowerHasGroupsLoading = false)))
        .subscribe((res: PowerHasGroup[]) => {
            this.powerPowerHasGroups = res;
        });
    }

    /** 权限 */
    initParentPowers() {
        this.parentPowersLoading = true;
        this.parentPowersVO.parentVO= this.powerVO;
        this.powerService
        .getAllByVO(this.parentPowersVO)
        .pipe(finalize(() => (this.parentPowersLoading = false)))
        .subscribe((res: Power[]) => {
            this.parentPowers = res;
        });
    }


}
