import { PowerHasGroup } from '../powerHasGroup/powerHasGroup';

/*
* Created by jiangchen
* Automated Build
* 实体 Power 权限
*/
export class Power {
    id: any;/**/
    parentId: any;/*父节点*/
    name: any;/*名称*/
    isDel: any;/*逻辑删除*/
    type: any;/*类型 1 展示类  2 操作类*/
    url: any;/*访问地址*/
    key: any;/*访问key*/
    parent: Power;/*父节点*/
    powerPowerHasGroups: PowerHasGroup[];/* 权限分组多对多*/
    parentPowers: Power[];/* 权限*/
}