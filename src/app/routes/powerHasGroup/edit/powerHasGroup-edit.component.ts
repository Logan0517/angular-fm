import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { PowerHasGroupService } from '../powerHasGroup.service';
import { PowerHasGroup } from '../powerHasGroup';
import { PowerHasGroupVO } from '../powerHasGroup.vo';
import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';
import { PowerService } from '../../power/power.service';
import { Power } from '../../power/power';
import { PowerVO } from '../../power/power.vo';

@Component({
  selector: 'app-powerhasgroup-edit',
  templateUrl: './powerhasgroup-edit.component.html',
})
export class PowerHasGroupEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private powerHasGroupService: PowerHasGroupService,
    private groupService: GroupService,
    private powerService: PowerService,
  ) {}

  form: FormGroup;
  loading = false;
  powerHasGroup: PowerHasGroup = new PowerHasGroup();
  powerHasGroupVO: PowerHasGroupVO = new PowerHasGroupVO();
  groups: Group[];
  groupVO: GroupVO;
  powers: Power[];
  powerVO: PowerVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.powerHasGroup = new PowerHasGroup();
    if(!this.powerHasGroupVO){
        this.powerHasGroupVO = new PowerHasGroupVO();
    }
    if(this.powerHasGroupVO.groupId && this.powerHasGroupVO.powerId){
      this.loading = false;
      this.powerHasGroupService
        .getByVo(this.powerHasGroupVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.powerHasGroup = res;
        });
    }
    this.groupVO = new GroupVO();
    this.groupService.getAllByVO(this.groupVO).subscribe((res: Group[]) => {
      this.groups = res;
    });
    this.powerVO = new PowerVO();
    this.powerService.getAllByVO(this.powerVO).subscribe((res: Power[]) => {
      this.powers = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        group: new FormControl({ name: 'group', value: '' }, Validators.required),/*用户*/
        power: new FormControl({ name: 'power', value: '' }, Validators.required),/*分组*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/*逻辑删除*/
    });
  }
    submit() {
        if (this.powerHasGroup.groupId && this.powerHasGroup.powerId) {
            this.powerHasGroupService
                .upd(this.powerHasGroup)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.powerHasGroupService
            .add(this.powerHasGroup)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
