import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { PowerHasGroupRoutingModule } from './powerHasGroup-routing.module';
import { PowerHasGroupListComponent } from './list/powerHasGroup-list.component';
import { PowerHasGroupViewComponent } from './view/powerHasGroup-view.component';
import { PowerHasGroupEditComponent } from './edit/powerHasGroup-edit.component';

const COMPONENTS = [PowerHasGroupListComponent,PowerHasGroupViewComponent];
const COMPONENTS_NOROUNT = [PowerHasGroupEditComponent];

@NgModule({
    imports: [SharedModule,PowerHasGroupRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class PowerHasGroupModule { }
