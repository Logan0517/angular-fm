import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PowerHasGroupListComponent } from './list/powerHasGroup-list.component';
import { PowerHasGroupViewComponent } from './view/powerHasGroup-view.component';

const routes: Routes = [
    { path: 'list', component: PowerHasGroupListComponent },
    { path: 'view/:groupId/:powerId', component: PowerHasGroupViewComponent, data: { title: '权限分组多对多'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PowerHasGroupRoutingModule { }
