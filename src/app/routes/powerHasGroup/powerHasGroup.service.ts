import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class PowerHasGroupService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'powerHasGroup/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'powerHasGroup/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'powerHasGroup/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'powerHasGroup/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'powerHasGroup/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'powerHasGroup/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'powerHasGroup/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'powerHasGroup/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'powerHasGroup/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'powerHasGroup/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'powerHasGroup/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'powerHasGroup/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'powerHasGroup/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'powerHasGroup/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'powerHasGroup/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'powerHasGroup/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'powerHasGroup/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'powerHasGroup/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'powerHasGroup/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'powerHasGroup/getForRelation';
        return this.http.post(url,vo);
    }
}
