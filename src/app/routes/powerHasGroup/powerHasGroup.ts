import { Group } from '../group/group';
import { Power } from '../power/power';

/*
* Created by jiangchen
* Automated Build
* 实体 PowerHasGroup 权限分组多对多
*/
export class PowerHasGroup {
    groupId: any;/*用户*/
    powerId: any;/*分组*/
    isDel: any;/*逻辑删除*/
    group: Group;/*用户*/
    power: Power;/*分组*/
}