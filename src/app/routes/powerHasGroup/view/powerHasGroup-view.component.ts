import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { PowerHasGroupService } from '../powerHasGroup.service';
import { PowerHasGroup } from '../powerHasGroup';
import { PowerHasGroupVO } from '../powerHasGroup.vo';
import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';
import { PowerService } from '../../power/power.service';
import { Power } from '../../power/power';
import { PowerVO } from '../../power/power.vo';


@Component({
    selector: 'app-powerhasgroup-view',
    templateUrl: './powerhasgroup-view.component.html',
})
export class PowerHasGroupViewComponent implements OnInit {

    loading = false;
    powerHasGroup: PowerHasGroup = new PowerHasGroup();
    powerHasGroupVO: PowerHasGroupVO = new PowerHasGroupVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private powerHasGroupService:PowerHasGroupService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: PowerHasGroup) => {
            this.powerHasGroupVO.groupId = param.groupId;
            this.powerHasGroupVO.powerId = param.powerId;
        });
        this.loading = true;
        this.powerHasGroupService
        .getByVo(this.powerHasGroupVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: PowerHasGroup) => {
            this.powerHasGroup = res;
        });
    }


}
