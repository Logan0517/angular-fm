import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { PowerHasGroupViewComponent } from '../view/powerHasGroup-view.component';
import { PowerHasGroupEditComponent } from '../edit/powerHasGroup-edit.component';
import { PowerHasGroupService } from '../powerHasGroup.service';
import { PowerHasGroup } from '../powerHasGroup';
import { PowerHasGroupVO } from '../powerHasGroup.vo';
import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';
import { PowerService } from '../../power/power.service';
import { Power } from '../../power/power';
import { PowerVO } from '../../power/power.vo';

@Component({
    selector: 'app-powerhasgroup-list',
    templateUrl: './powerhasgroup-list.component.html',
    styles: [],
    providers: [PowerHasGroupEditComponent,PowerHasGroupViewComponent],
})
export class PowerHasGroupListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private powerHasGroupService: PowerHasGroupService,
    private groupService: GroupService,
    private powerService: PowerService,
  ) {}

  powerHasGroup: PowerHasGroup;
  powerHasGroupVO: PowerHasGroupVO;
  powerHasGroupVOs: PowerHasGroupVO[];
  powerHasGroups: PowerHasGroup[];
  groups: Group[];
  groupVO: GroupVO;
  powers: Power[];
  powerVO: PowerVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '用户', index: 'groupId' },
    { title: '分组', index: 'powerId' },
    { title: '逻辑删除', index: 'isDel' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: PowerHasGroup) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: PowerHasGroup) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: PowerHasGroup) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.powerHasGroupVO = new PowerHasGroupVO();
    this.powerHasGroupVO.page = 1;
    this.powerHasGroupVO.pageSize = 15;
    this.groupVO = new GroupVO();
    this.groupService.getAllByVO(this.groupVO).subscribe((res: Group[]) => {
      this.groups = res;
    });
    this.powerVO = new PowerVO();
    this.powerService.getAllByVO(this.powerVO).subscribe((res: Power[]) => {
      this.powers = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      group: new FormControl({ name: 'group', value: '' }),/*用户*/
      power: new FormControl({ name: 'power', value: '' }),/*分组*/
    });
  }

  search() {
    this.loading = true;
    this.powerHasGroupService
      .getByPage(this.powerHasGroupVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.powerHasGroups = res.datas;
        this.powerHasGroupVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.powerHasGroupVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: PowerHasGroup) {
    this.modal.createStatic(PowerHasGroupEditComponent, { powerHasGroupVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: PowerHasGroup) {
    this.loading = true;
    this.powerHasGroupService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: PowerHasGroup) {
    this.router.navigate(['/powerHasGroup/view/'+ item.groupId+'/'+item.powerId]);
  }

  removeMul() {
    this.loading = true;
    this.powerHasGroupVOs = [];
    this.selectedRows.forEach(selectRow => {
      const powerHasGroupTmpVO: PowerHasGroupVO = new PowerHasGroupVO();
      powerHasGroupTmpVO.groupId = selectRow.groupId;
      powerHasGroupTmpVO.powerId = selectRow.powerId;
      this.powerHasGroupVOs.push(powerHasGroupTmpVO);
    });
    this.powerHasGroupService
    .delByVOForLogicMul(this.powerHasGroupVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.powerHasGroupVOs = [];
    this.selectedRows.forEach(selectRow => {
    const powerHasGroupTmpVO: PowerHasGroupVO = new PowerHasGroupVO();
      powerHasGroupTmpVO.groupId = selectRow.groupId;
      powerHasGroupTmpVO.powerId = selectRow.powerId;
    this.powerHasGroupVOs.push(powerHasGroupTmpVO);
    });
    this.powerHasGroupService
    .updMul(this.powerHasGroupVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
