import { GroupVO } from '../group/group.vo';
import { PowerVO } from '../power/power.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 PowerHasGroup 权限分组多对多
*/
export class PowerHasGroupVO {
    groupId: any;/*用户*/
    powerId: any;/*分组*/
    isDel: any;/*逻辑删除*/
    groupVO: GroupVO;/*用户*/
    powerVO: PowerVO;/*分组*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}