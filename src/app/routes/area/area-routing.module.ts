import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreaListComponent } from './list/area-list.component';
import { AreaViewComponent } from './view/area-view.component';

const routes: Routes = [
    { path: 'list', component: AreaListComponent },
    { path: 'view/:id', component: AreaViewComponent, data: { title: '区域'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AreaRoutingModule { }
