import { Device } from '../device/device';
import { ManyTable } from '../manyTable/manyTable';
import { AccountHasArea } from '../accountHasArea/accountHasArea';

/*
* Created by jiangchen
* Automated Build
* 实体 Area 区域
*/
export class Area {
    id: any;/**/
    name: any;/*名称*/
    isDel: any;/**/
    centerLat: any;/*纬度*/
    centerLng: any;/*经度*/
    parentId: any;/*父节点*/
    parent: Area;/*父节点*/
    parentAreas: Area[];/* 区域*/
    areaDevices: Device[];/* 设备*/
    areaManyTables: ManyTable[];/* 测试多对多表*/
    areaAccountHasAreas: AccountHasArea[];/* */
}