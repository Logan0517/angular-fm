import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { AreaService } from '../area.service';
import { Area } from '../area';
import { AreaVO } from '../area.vo';

import { DeviceService } from '../../device/device.service';
import { Device } from '../../device/device';
import { DeviceVO } from '../../device/device.vo';
import { ManyTableService } from '../../manyTable/manyTable.service';
import { ManyTable } from '../../manyTable/manyTable';
import { ManyTableVO } from '../../manyTable/manyTable.vo';
import { AccountHasAreaService } from '../../accountHasArea/accountHasArea.service';
import { AccountHasArea } from '../../accountHasArea/accountHasArea';
import { AccountHasAreaVO } from '../../accountHasArea/accountHasArea.vo';

@Component({
    selector: 'app-area-view',
    templateUrl: './area-view.component.html',
})
export class AreaViewComponent implements OnInit {

    loading = false;
    area: Area = new Area();
    areaVO: AreaVO = new AreaVO();
    /** 区域 */
    parentAreas: Area[] = [];
    parentAreasVO: AreaVO = new AreaVO();
    parentAreasLoading = false;
    /** 设备 */
    areaDevices: Device[] = [];
    areaDevicesVO: DeviceVO = new DeviceVO();
    areaDevicesLoading = false;
    /** 测试多对多表 */
    areaManyTables: ManyTable[] = [];
    areaManyTablesVO: ManyTableVO = new ManyTableVO();
    areaManyTablesLoading = false;
    /**  */
    areaAccountHasAreas: AccountHasArea[] = [];
    areaAccountHasAreasVO: AccountHasAreaVO = new AccountHasAreaVO();
    areaAccountHasAreasLoading = false;

    /** 区域 */
    parentAreasColumns: STColumn[] = [
        { title: '纬度', index: 'centerLat', default: '-'},
        { title: '父节点', index: 'parentId', default: '-'},
        { title: '名称', index: 'name', default: '-'},
        { title: '', index: 'isDel', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '经度', index: 'centerLng', default: '-'},
    ];
    /** 设备 */
    areaDevicesColumns: STColumn[] = [
        { title: '系统code', index: 'code', default: '-'},
        { title: '分组数量', index: 'groupCount', default: '-'},
        { title: '纬度', index: 'gpsLat', default: '-'},
        { title: '设备类型', index: 'type', default: '-'},
        { title: '所在区域', index: 'areaId', default: '-'},
        { title: '经度', index: 'gpsLng', default: '-'},
        { title: '设备编码', index: 'deviceCode', default: '-'},
        { title: '设备名称', index: 'name', default: '-'},
        { title: '', index: 'isDel', default: '-'},
        { title: '探头数量', index: 'probeCount', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '状态', index: 'status', default: '-'},
    ];
    /** 测试多对多表 */
    areaManyTablesColumns: STColumn[] = [
        { title: '', index: 'accountId', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '', index: 'areaId', default: '-'},
    ];
    /**  */
    areaAccountHasAreasColumns: STColumn[] = [
        { title: '', index: 'areaId', default: '-'},
        { title: '', index: 'accountId', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private areaService:AreaService,
        private deviceService:DeviceService,
        private manyTableService:ManyTableService,
        private accountHasAreaService:AccountHasAreaService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initParentAreas();
        this.initAreaDevices();
        this.initAreaManyTables();
        this.initAreaAccountHasAreas();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Area) => {
            this.areaVO.id = param.id;
        });
        this.loading = true;
        this.areaService
        .getByVo(this.areaVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Area) => {
            this.area = res;
        });
    }

    /** 区域 */
    initParentAreas() {
        this.parentAreasLoading = true;
        this.parentAreasVO.parentVO= this.areaVO;
        this.areaService
        .getAllByVO(this.parentAreasVO)
        .pipe(finalize(() => (this.parentAreasLoading = false)))
        .subscribe((res: Area[]) => {
            this.parentAreas = res;
        });
    }

    /** 设备 */
    initAreaDevices() {
        this.areaDevicesLoading = true;
        this.areaDevicesVO.areaVO= this.areaVO;
        this.deviceService
        .getAllByVO(this.areaDevicesVO)
        .pipe(finalize(() => (this.areaDevicesLoading = false)))
        .subscribe((res: Device[]) => {
            this.areaDevices = res;
        });
    }

    /** 测试多对多表 */
    initAreaManyTables() {
        this.areaManyTablesLoading = true;
        this.areaManyTablesVO.areaVO= this.areaVO;
        this.manyTableService
        .getAllByVO(this.areaManyTablesVO)
        .pipe(finalize(() => (this.areaManyTablesLoading = false)))
        .subscribe((res: ManyTable[]) => {
            this.areaManyTables = res;
        });
    }

    /**  */
    initAreaAccountHasAreas() {
        this.areaAccountHasAreasLoading = true;
        this.areaAccountHasAreasVO.areaVO= this.areaVO;
        this.accountHasAreaService
        .getAllByVO(this.areaAccountHasAreasVO)
        .pipe(finalize(() => (this.areaAccountHasAreasLoading = false)))
        .subscribe((res: AccountHasArea[]) => {
            this.areaAccountHasAreas = res;
        });
    }


}
