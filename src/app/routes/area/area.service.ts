import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class AreaService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'area/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'area/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'area/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'area/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'area/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'area/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'area/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'area/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'area/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'area/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'area/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'area/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'area/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'area/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'area/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'area/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'area/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'area/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'area/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'area/getForRelation';
        return this.http.post(url,vo);
    }
}
