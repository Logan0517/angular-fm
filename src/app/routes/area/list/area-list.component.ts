import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { AreaViewComponent } from '../view/area-view.component';
import { AreaEditComponent } from '../edit/area-edit.component';
import { AreaService } from '../area.service';
import { Area } from '../area';
import { AreaVO } from '../area.vo';

@Component({
    selector: 'app-area-list',
    templateUrl: './area-list.component.html',
    styles: [],
    providers: [AreaEditComponent,AreaViewComponent],
})
export class AreaListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private areaService: AreaService,
  ) {}

  area: Area;
  areaVO: AreaVO;
  areaVOs: AreaVO[];
  areas: Area[];
  parents: Area[];
  parentVO: AreaVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '名称', index: 'name' },
    { title: '', index: 'isDel' },
    { title: '纬度', index: 'centerLat' },
    { title: '经度', index: 'centerLng' },
    { title: '父节点', index: 'parentId' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Area) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Area) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Area) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.areaVO = new AreaVO();
    this.areaVO.page = 1;
    this.areaVO.pageSize = 15;
    this.parentVO = new AreaVO();
    this.areaService.getAllByVO(this.parentVO).subscribe((res: Area[]) => {
      this.parents = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*名称模糊查询*/
      parent: new FormControl({ name: 'parent', value: '' }),/*父节点*/
    });
  }

  search() {
    this.loading = true;
    this.areaService
      .getByPage(this.areaVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.areas = res.datas;
        this.areaVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.areaVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Area) {
    this.modal.createStatic(AreaEditComponent, { areaVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Area) {
    this.loading = true;
    this.areaService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Area) {
    this.router.navigate(['/area/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.areaVOs = [];
    this.selectedRows.forEach(selectRow => {
      const areaTmpVO: AreaVO = new AreaVO();
      areaTmpVO.id = selectRow.id;
      this.areaVOs.push(areaTmpVO);
    });
    this.areaService
    .delByVOForLogicMul(this.areaVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.areaVOs = [];
    this.selectedRows.forEach(selectRow => {
    const areaTmpVO: AreaVO = new AreaVO();
      areaTmpVO.id = selectRow.id;
    this.areaVOs.push(areaTmpVO);
    });
    this.areaService
    .updMul(this.areaVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
