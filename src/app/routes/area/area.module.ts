import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { AreaRoutingModule } from './area-routing.module';
import { AreaListComponent } from './list/area-list.component';
import { AreaViewComponent } from './view/area-view.component';
import { AreaEditComponent } from './edit/area-edit.component';

const COMPONENTS = [AreaListComponent,AreaViewComponent];
const COMPONENTS_NOROUNT = [AreaEditComponent];

@NgModule({
    imports: [SharedModule,AreaRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class AreaModule { }
