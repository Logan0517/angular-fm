import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { AreaService } from '../area.service';
import { Area } from '../area';
import { AreaVO } from '../area.vo';

@Component({
  selector: 'app-area-edit',
  templateUrl: './area-edit.component.html',
})
export class AreaEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private areaService: AreaService,
  ) {}

  form: FormGroup;
  loading = false;
  area: Area = new Area();
  areaVO: AreaVO = new AreaVO();
  parents: Area[];
  parentVO: AreaVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.area = new Area();
    if(!this.areaVO){
        this.areaVO = new AreaVO();
    }
    if(this.areaVO.id){
      this.loading = false;
      this.areaService
        .getByVo(this.areaVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.area = res;
        });
    }
    this.parentVO = new AreaVO();
    this.areaService.getAllByVO(this.parentVO).subscribe((res: Area[]) => {
      this.parents = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        parent: new FormControl({ name: 'parent', value: '' }, Validators.required),/*父节点*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        centerLat: new FormControl({ name: 'centerLat', value: '' }, [Validators.required]),/*纬度*/
        centerLng: new FormControl({ name: 'centerLng', value: '' }, [Validators.required]),/*经度*/
        parentId: new FormControl({ name: 'parentId', value: '' }, [Validators.required]),/*父节点*/
    });
  }
    submit() {
        if (this.area.id) {
            this.areaService
                .upd(this.area)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.areaService
            .add(this.area)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
