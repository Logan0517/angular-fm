import { DeviceVO } from '../device/device.vo';
import { ManyTableVO } from '../manyTable/manyTable.vo';
import { AccountHasAreaVO } from '../accountHasArea/accountHasArea.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Area 区域
*/
export class AreaVO {
    id: any;/**/
    name: any;/*名称*/
    nameLike: any;/*名称模糊查询*/
    isDel: any;/**/
    centerLat: any;/*纬度*/
    centerLng: any;/*经度*/
    parentId: any;/*父节点*/
    parentVO: AreaVO;/*父节点*/
    parentAreasVO: AreaVO[];/* 区域*/
    areaDevicesVO: DeviceVO[];/* 设备*/
    areaManyTablesVO: ManyTableVO[];/* 测试多对多表*/
    areaAccountHasAreasVO: AccountHasAreaVO[];/* */
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}