import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { ProbeGroupService } from '../probeGroup.service';
import { ProbeGroup } from '../probeGroup';
import { ProbeGroupVO } from '../probeGroup.vo';
import { DeviceService } from '../../device/device.service';
import { Device } from '../../device/device';
import { DeviceVO } from '../../device/device.vo';

@Component({
  selector: 'app-probegroup-edit',
  templateUrl: './probegroup-edit.component.html',
})
export class ProbeGroupEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private probeGroupService: ProbeGroupService,
    private deviceService: DeviceService,
  ) {}

  form: FormGroup;
  loading = false;
  probeGroup: ProbeGroup = new ProbeGroup();
  probeGroupVO: ProbeGroupVO = new ProbeGroupVO();
  devices: Device[];
  deviceVO: DeviceVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.probeGroup = new ProbeGroup();
    if(!this.probeGroupVO){
        this.probeGroupVO = new ProbeGroupVO();
    }
    if(this.probeGroupVO.id){
      this.loading = false;
      this.probeGroupService
        .getByVo(this.probeGroupVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.probeGroup = res;
        });
    }
    this.deviceVO = new DeviceVO();
    this.deviceService.getAllByVO(this.deviceVO).subscribe((res: Device[]) => {
      this.devices = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        device: new FormControl({ name: 'device', value: '' }, Validators.required),/*设备编码*/
        code: new FormControl({ name: 'code', value: '' }, [Validators.required]),/*编码*/
        deviceCode: new FormControl({ name: 'deviceCode', value: '' }, [Validators.required]),/*设备编码*/
        sortNum: new FormControl({ name: 'sortNum', value: '' }, [Validators.required]),/*排序编号 从1开始*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*分组名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        deviceBusCode: new FormControl({ name: 'deviceBusCode', value: '' }, [Validators.required]),/*设备业务编码*/
    });
  }
    submit() {
        if (this.probeGroup.id) {
            this.probeGroupService
                .upd(this.probeGroup)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.probeGroupService
            .add(this.probeGroup)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
