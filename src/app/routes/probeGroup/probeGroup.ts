import { Device } from '../device/device';
import { Probe } from '../probe/probe';

/*
* Created by jiangchen
* Automated Build
* 实体 ProbeGroup 探头分组
*/
export class ProbeGroup {
    id: any;/**/
    code: any;/*编码*/
    deviceCode: any;/*设备编码*/
    sortNum: any;/*排序编号 从1开始*/
    name: any;/*分组名称*/
    isDel: any;/**/
    deviceBusCode: any;/*设备业务编码*/
    device: Device;/*设备编码*/
    groupProbes: Probe[];/* 探头*/
}