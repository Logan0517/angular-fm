import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProbeGroupListComponent } from './list/probeGroup-list.component';
import { ProbeGroupViewComponent } from './view/probeGroup-view.component';

const routes: Routes = [
    { path: 'list', component: ProbeGroupListComponent },
    { path: 'view/:id', component: ProbeGroupViewComponent, data: { title: '探头分组'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProbeGroupRoutingModule { }
