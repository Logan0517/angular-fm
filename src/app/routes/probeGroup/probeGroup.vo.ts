import { DeviceVO } from '../device/device.vo';
import { ProbeVO } from '../probe/probe.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 ProbeGroup 探头分组
*/
export class ProbeGroupVO {
    id: any;/**/
    code: any;/*编码*/
    deviceCode: any;/*设备编码*/
    sortNum: any;/*排序编号 从1开始*/
    name: any;/*分组名称*/
    nameLike: any;/*分组名称模糊查询*/
    isDel: any;/**/
    deviceBusCode: any;/*设备业务编码*/
    deviceVO: DeviceVO;/*设备编码*/
    groupProbesVO: ProbeVO[];/* 探头*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}