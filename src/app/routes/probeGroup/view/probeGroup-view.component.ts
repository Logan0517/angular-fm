import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { ProbeGroupService } from '../probeGroup.service';
import { ProbeGroup } from '../probeGroup';
import { ProbeGroupVO } from '../probeGroup.vo';
import { DeviceService } from '../../device/device.service';
import { Device } from '../../device/device';
import { DeviceVO } from '../../device/device.vo';

import { ProbeService } from '../../probe/probe.service';
import { Probe } from '../../probe/probe';
import { ProbeVO } from '../../probe/probe.vo';

@Component({
    selector: 'app-probegroup-view',
    templateUrl: './probegroup-view.component.html',
})
export class ProbeGroupViewComponent implements OnInit {

    loading = false;
    probeGroup: ProbeGroup = new ProbeGroup();
    probeGroupVO: ProbeGroupVO = new ProbeGroupVO();
    /** 探头 */
    groupProbes: Probe[] = [];
    groupProbesVO: ProbeVO = new ProbeVO();
    groupProbesLoading = false;

    /** 探头 */
    groupProbesColumns: STColumn[] = [
        { title: '编码', index: 'code', default: '-'},
        { title: '分组', index: 'groupCode', default: '-'},
        { title: '分组序号 0代表没有分组', index: 'groupNum', default: '-'},
        { title: '设备编码', index: 'deviceCode', default: '-'},
        { title: '排序编号 从1开始', index: 'sortNum', default: '-'},
        { title: '名称', index: 'name', default: '-'},
        { title: '', index: 'isDel', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '设备业务编码', index: 'deviceBusCode', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private probeGroupService:ProbeGroupService,
        private probeService:ProbeService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initGroupProbes();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: ProbeGroup) => {
            this.probeGroupVO.id = param.id;
        });
        this.loading = true;
        this.probeGroupService
        .getByVo(this.probeGroupVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: ProbeGroup) => {
            this.probeGroup = res;
        });
    }

    /** 探头 */
    initGroupProbes() {
        this.groupProbesLoading = true;
        this.groupProbesVO.groupVO= this.probeGroupVO;
        this.probeService
        .getAllByVO(this.groupProbesVO)
        .pipe(finalize(() => (this.groupProbesLoading = false)))
        .subscribe((res: Probe[]) => {
            this.groupProbes = res;
        });
    }


}
