import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ProbeGroupRoutingModule } from './probeGroup-routing.module';
import { ProbeGroupListComponent } from './list/probeGroup-list.component';
import { ProbeGroupViewComponent } from './view/probeGroup-view.component';
import { ProbeGroupEditComponent } from './edit/probeGroup-edit.component';

const COMPONENTS = [ProbeGroupListComponent,ProbeGroupViewComponent];
const COMPONENTS_NOROUNT = [ProbeGroupEditComponent];

@NgModule({
    imports: [SharedModule,ProbeGroupRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class ProbeGroupModule { }
