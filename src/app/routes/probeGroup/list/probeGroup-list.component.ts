import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { ProbeGroupViewComponent } from '../view/probeGroup-view.component';
import { ProbeGroupEditComponent } from '../edit/probeGroup-edit.component';
import { ProbeGroupService } from '../probeGroup.service';
import { ProbeGroup } from '../probeGroup';
import { ProbeGroupVO } from '../probeGroup.vo';
import { DeviceService } from '../../device/device.service';
import { Device } from '../../device/device';
import { DeviceVO } from '../../device/device.vo';

@Component({
    selector: 'app-probegroup-list',
    templateUrl: './probegroup-list.component.html',
    styles: [],
    providers: [ProbeGroupEditComponent,ProbeGroupViewComponent],
})
export class ProbeGroupListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private probeGroupService: ProbeGroupService,
    private deviceService: DeviceService,
  ) {}

  probeGroup: ProbeGroup;
  probeGroupVO: ProbeGroupVO;
  probeGroupVOs: ProbeGroupVO[];
  probeGroups: ProbeGroup[];
  devices: Device[];
  deviceVO: DeviceVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '编码', index: 'code' },
    { title: '设备编码', index: 'deviceCode' },
    { title: '排序编号 从1开始', index: 'sortNum' },
    { title: '分组名称', index: 'name' },
    { title: '', index: 'isDel' },
    { title: '设备业务编码', index: 'deviceBusCode' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: ProbeGroup) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: ProbeGroup) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: ProbeGroup) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.probeGroupVO = new ProbeGroupVO();
    this.probeGroupVO.page = 1;
    this.probeGroupVO.pageSize = 15;
    this.deviceVO = new DeviceVO();
    this.deviceService.getAllByVO(this.deviceVO).subscribe((res: Device[]) => {
      this.devices = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*分组名称模糊查询*/
      device: new FormControl({ name: 'device', value: '' }),/*设备编码*/
    });
  }

  search() {
    this.loading = true;
    this.probeGroupService
      .getByPage(this.probeGroupVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.probeGroups = res.datas;
        this.probeGroupVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.probeGroupVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: ProbeGroup) {
    this.modal.createStatic(ProbeGroupEditComponent, { probeGroupVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: ProbeGroup) {
    this.loading = true;
    this.probeGroupService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: ProbeGroup) {
    this.router.navigate(['/probeGroup/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.probeGroupVOs = [];
    this.selectedRows.forEach(selectRow => {
      const probeGroupTmpVO: ProbeGroupVO = new ProbeGroupVO();
      probeGroupTmpVO.id = selectRow.id;
      this.probeGroupVOs.push(probeGroupTmpVO);
    });
    this.probeGroupService
    .delByVOForLogicMul(this.probeGroupVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.probeGroupVOs = [];
    this.selectedRows.forEach(selectRow => {
    const probeGroupTmpVO: ProbeGroupVO = new ProbeGroupVO();
      probeGroupTmpVO.id = selectRow.id;
    this.probeGroupVOs.push(probeGroupTmpVO);
    });
    this.probeGroupService
    .updMul(this.probeGroupVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
