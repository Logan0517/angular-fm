import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class ProbeGroupService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'probeGroup/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'probeGroup/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'probeGroup/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'probeGroup/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'probeGroup/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'probeGroup/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'probeGroup/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'probeGroup/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'probeGroup/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'probeGroup/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'probeGroup/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'probeGroup/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'probeGroup/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'probeGroup/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'probeGroup/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'probeGroup/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'probeGroup/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'probeGroup/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'probeGroup/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'probeGroup/getForRelation';
        return this.http.post(url,vo);
    }
}
