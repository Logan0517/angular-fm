import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { AccountViewComponent } from '../view/account-view.component';
import { AccountEditComponent } from '../edit/account-edit.component';
import { AccountService } from '../account.service';
import { Account } from '../account';
import { AccountVO } from '../account.vo';

@Component({
    selector: 'app-account-list',
    templateUrl: './account-list.component.html',
    styles: [],
    providers: [AccountEditComponent,AccountViewComponent],
})
export class AccountListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private accountService: AccountService,
  ) {}

  account: Account;
  accountVO: AccountVO;
  accountVOs: AccountVO[];
  accounts: Account[];
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: 'Id标识', index: 'id' },
    { title: '密码', index: 'password' },
    { title: '编码', index: 'code' },
    { title: '手机号', index: 'mobile' },
    { title: '姓名', index: 'name' },
    { title: '逻辑删除', index: 'isDel' },
    { title: '账号', index: 'account' },
    { title: '状态 0 正常 1 冻结', index: 'status' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Account) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Account) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Account) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.accountVO = new AccountVO();
    this.accountVO.page = 1;
    this.accountVO.pageSize = 15;
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      mobileLike: new FormControl({ name: 'mobileLike', value: '' }),/*手机号模糊查询*/
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*姓名模糊查询*/
    });
  }

  search() {
    this.loading = true;
    this.accountService
      .getByPage(this.accountVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.accounts = res.datas;
        this.accountVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.accountVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Account) {
    this.modal.createStatic(AccountEditComponent, { accountVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Account) {
    this.loading = true;
    this.accountService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Account) {
    this.router.navigate(['/account/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.accountVOs = [];
    this.selectedRows.forEach(selectRow => {
      const accountTmpVO: AccountVO = new AccountVO();
      accountTmpVO.id = selectRow.id;
      this.accountVOs.push(accountTmpVO);
    });
    this.accountService
    .delByVOForLogicMul(this.accountVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.accountVOs = [];
    this.selectedRows.forEach(selectRow => {
    const accountTmpVO: AccountVO = new AccountVO();
      accountTmpVO.id = selectRow.id;
    this.accountVOs.push(accountTmpVO);
    });
    this.accountService
    .updMul(this.accountVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
