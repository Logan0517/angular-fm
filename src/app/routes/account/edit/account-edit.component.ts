import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { AccountService } from '../account.service';
import { Account } from '../account';
import { AccountVO } from '../account.vo';

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
})
export class AccountEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private accountService: AccountService,
  ) {}

  form: FormGroup;
  loading = false;
  account: Account = new Account();
  accountVO: AccountVO = new AccountVO();

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.account = new Account();
    if(!this.accountVO){
        this.accountVO = new AccountVO();
    }
    if(this.accountVO.id){
      this.loading = false;
      this.accountService
        .getByVo(this.accountVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.account = res;
        });
    }
  }

  initForm() {
    this.form = new FormGroup({
        password: new FormControl({ name: 'password', value: '' }, [Validators.required]),/*密码*/
        code: new FormControl({ name: 'code', value: '' }, [Validators.required]),/*编码*/
        mobile: new FormControl({ name: 'mobile', value: '' }, [Validators.required]),/*手机号*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*姓名*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/*逻辑删除*/
        account: new FormControl({ name: 'account', value: '' }, [Validators.required]),/*账号*/
        status: new FormControl({ name: 'status', value: '' }, [Validators.required]),/*状态 0 正常 1 冻结*/
    });
  }
    submit() {
        if (this.account.id) {
            this.accountService
                .upd(this.account)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.accountService
            .add(this.account)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
