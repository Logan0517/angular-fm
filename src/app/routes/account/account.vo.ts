import { GroupHasAccountVO } from '../groupHasAccount/groupHasAccount.vo';
import { ManyTableVO } from '../manyTable/manyTable.vo';
import { AccountHasAreaVO } from '../accountHasArea/accountHasArea.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Account 用户账号
*/
export class AccountVO {
    id: any;/*Id标识*/
    password: any;/*密码*/
    code: any;/*编码*/
    mobile: any;/*手机号*/
    mobileLike: any;/*手机号模糊查询*/
    name: any;/*姓名*/
    nameLike: any;/*姓名模糊查询*/
    isDel: any;/*逻辑删除*/
    account: any;/*账号*/
    status: any;/*状态 0 正常 1 冻结*/
    accountGroupHasAccountsVO: GroupHasAccountVO[];/* 用户分组多对多*/
    accountManyTablesVO: ManyTableVO[];/* 测试多对多表*/
    accountAccountHasAreasVO: AccountHasAreaVO[];/* */
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}