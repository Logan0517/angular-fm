import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { AccountRoutingModule } from './account-routing.module';
import { AccountListComponent } from './list/account-list.component';
import { AccountViewComponent } from './view/account-view.component';
import { AccountEditComponent } from './edit/account-edit.component';

const COMPONENTS = [AccountListComponent,AccountViewComponent];
const COMPONENTS_NOROUNT = [AccountEditComponent];

@NgModule({
    imports: [SharedModule,AccountRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class AccountModule { }
