import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class AccountService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'account/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'account/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'account/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'account/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'account/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'account/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'account/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'account/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'account/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'account/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'account/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'account/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'account/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'account/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'account/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'account/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'account/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'account/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'account/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'account/getForRelation';
        return this.http.post(url,vo);
    }
}
