import { GroupHasAccount } from '../groupHasAccount/groupHasAccount';
import { ManyTable } from '../manyTable/manyTable';
import { AccountHasArea } from '../accountHasArea/accountHasArea';

/*
* Created by jiangchen
* Automated Build
* 实体 Account 用户账号
*/
export class Account {
    id: any;/*Id标识*/
    password: any;/*密码*/
    code: any;/*编码*/
    mobile: any;/*手机号*/
    name: any;/*姓名*/
    isDel: any;/*逻辑删除*/
    account: any;/*账号*/
    status: any;/*状态 0 正常 1 冻结*/
    accountGroupHasAccounts: GroupHasAccount[];/* 用户分组多对多*/
    accountManyTables: ManyTable[];/* 测试多对多表*/
    accountAccountHasAreas: AccountHasArea[];/* */
}