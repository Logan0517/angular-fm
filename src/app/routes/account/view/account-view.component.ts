import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { AccountService } from '../account.service';
import { Account } from '../account';
import { AccountVO } from '../account.vo';

import { GroupHasAccountService } from '../../groupHasAccount/groupHasAccount.service';
import { GroupHasAccount } from '../../groupHasAccount/groupHasAccount';
import { GroupHasAccountVO } from '../../groupHasAccount/groupHasAccount.vo';
import { ManyTableService } from '../../manyTable/manyTable.service';
import { ManyTable } from '../../manyTable/manyTable';
import { ManyTableVO } from '../../manyTable/manyTable.vo';
import { AccountHasAreaService } from '../../accountHasArea/accountHasArea.service';
import { AccountHasArea } from '../../accountHasArea/accountHasArea';
import { AccountHasAreaVO } from '../../accountHasArea/accountHasArea.vo';

@Component({
    selector: 'app-account-view',
    templateUrl: './account-view.component.html',
})
export class AccountViewComponent implements OnInit {

    loading = false;
    account: Account = new Account();
    accountVO: AccountVO = new AccountVO();
    /** 用户分组多对多 */
    accountGroupHasAccounts: GroupHasAccount[] = [];
    accountGroupHasAccountsVO: GroupHasAccountVO = new GroupHasAccountVO();
    accountGroupHasAccountsLoading = false;
    /** 测试多对多表 */
    accountManyTables: ManyTable[] = [];
    accountManyTablesVO: ManyTableVO = new ManyTableVO();
    accountManyTablesLoading = false;
    /**  */
    accountAccountHasAreas: AccountHasArea[] = [];
    accountAccountHasAreasVO: AccountHasAreaVO = new AccountHasAreaVO();
    accountAccountHasAreasLoading = false;

    /** 用户分组多对多 */
    accountGroupHasAccountsColumns: STColumn[] = [
        { title: '分组', index: 'groupId', default: '-'},
        { title: '用户', index: 'accountId', default: '-'},
    ];
    /** 测试多对多表 */
    accountManyTablesColumns: STColumn[] = [
        { title: '', index: 'accountId', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '', index: 'areaId', default: '-'},
    ];
    /**  */
    accountAccountHasAreasColumns: STColumn[] = [
        { title: '', index: 'areaId', default: '-'},
        { title: '', index: 'accountId', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private accountService:AccountService,
        private groupHasAccountService:GroupHasAccountService,
        private manyTableService:ManyTableService,
        private accountHasAreaService:AccountHasAreaService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initAccountGroupHasAccounts();
        this.initAccountManyTables();
        this.initAccountAccountHasAreas();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Account) => {
            this.accountVO.id = param.id;
        });
        this.loading = true;
        this.accountService
        .getByVo(this.accountVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Account) => {
            this.account = res;
        });
    }

    /** 用户分组多对多 */
    initAccountGroupHasAccounts() {
        this.accountGroupHasAccountsLoading = true;
        this.accountGroupHasAccountsVO.accountVO= this.accountVO;
        this.groupHasAccountService
        .getAllByVO(this.accountGroupHasAccountsVO)
        .pipe(finalize(() => (this.accountGroupHasAccountsLoading = false)))
        .subscribe((res: GroupHasAccount[]) => {
            this.accountGroupHasAccounts = res;
        });
    }

    /** 测试多对多表 */
    initAccountManyTables() {
        this.accountManyTablesLoading = true;
        this.accountManyTablesVO.accountVO= this.accountVO;
        this.manyTableService
        .getAllByVO(this.accountManyTablesVO)
        .pipe(finalize(() => (this.accountManyTablesLoading = false)))
        .subscribe((res: ManyTable[]) => {
            this.accountManyTables = res;
        });
    }

    /**  */
    initAccountAccountHasAreas() {
        this.accountAccountHasAreasLoading = true;
        this.accountAccountHasAreasVO.accountVO= this.accountVO;
        this.accountHasAreaService
        .getAllByVO(this.accountAccountHasAreasVO)
        .pipe(finalize(() => (this.accountAccountHasAreasLoading = false)))
        .subscribe((res: AccountHasArea[]) => {
            this.accountAccountHasAreas = res;
        });
    }


}
