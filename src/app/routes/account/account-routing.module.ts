import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountListComponent } from './list/account-list.component';
import { AccountViewComponent } from './view/account-view.component';

const routes: Routes = [
    { path: 'list', component: AccountListComponent },
    { path: 'view/:id', component: AccountViewComponent, data: { title: '用户账号'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AccountRoutingModule { }
