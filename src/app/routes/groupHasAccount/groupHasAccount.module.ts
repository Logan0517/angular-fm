import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { GroupHasAccountRoutingModule } from './groupHasAccount-routing.module';
import { GroupHasAccountListComponent } from './list/groupHasAccount-list.component';
import { GroupHasAccountViewComponent } from './view/groupHasAccount-view.component';
import { GroupHasAccountEditComponent } from './edit/groupHasAccount-edit.component';

const COMPONENTS = [GroupHasAccountListComponent,GroupHasAccountViewComponent];
const COMPONENTS_NOROUNT = [GroupHasAccountEditComponent];

@NgModule({
    imports: [SharedModule,GroupHasAccountRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class GroupHasAccountModule { }
