import { Group } from '../group/group';
import { Account } from '../account/account';

/*
* Created by jiangchen
* Automated Build
* 实体 GroupHasAccount 用户分组多对多
*/
export class GroupHasAccount {
    groupId: any;/*分组*/
    accountId: any;/*用户*/
    group: Group;/*分组*/
    account: Account;/*用户*/
}