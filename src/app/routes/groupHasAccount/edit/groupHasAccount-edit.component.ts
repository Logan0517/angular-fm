import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { GroupHasAccountService } from '../groupHasAccount.service';
import { GroupHasAccount } from '../groupHasAccount';
import { GroupHasAccountVO } from '../groupHasAccount.vo';
import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';

@Component({
  selector: 'app-grouphasaccount-edit',
  templateUrl: './grouphasaccount-edit.component.html',
})
export class GroupHasAccountEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private groupHasAccountService: GroupHasAccountService,
    private groupService: GroupService,
    private accountService: AccountService,
  ) {}

  form: FormGroup;
  loading = false;
  groupHasAccount: GroupHasAccount = new GroupHasAccount();
  groupHasAccountVO: GroupHasAccountVO = new GroupHasAccountVO();
  groups: Group[];
  groupVO: GroupVO;
  accounts: Account[];
  accountVO: AccountVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.groupHasAccount = new GroupHasAccount();
    if(!this.groupHasAccountVO){
        this.groupHasAccountVO = new GroupHasAccountVO();
    }
    if(this.groupHasAccountVO.groupId && this.groupHasAccountVO.accountId){
      this.loading = false;
      this.groupHasAccountService
        .getByVo(this.groupHasAccountVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.groupHasAccount = res;
        });
    }
    this.groupVO = new GroupVO();
    this.groupService.getAllByVO(this.groupVO).subscribe((res: Group[]) => {
      this.groups = res;
    });
    this.accountVO = new AccountVO();
    this.accountService.getAllByVO(this.accountVO).subscribe((res: Account[]) => {
      this.accounts = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        group: new FormControl({ name: 'group', value: '' }, Validators.required),/*分组*/
        account: new FormControl({ name: 'account', value: '' }, Validators.required),/*用户*/
    });
  }
    submit() {
        if (this.groupHasAccount.groupId && this.groupHasAccount.accountId) {
            this.groupHasAccountService
                .upd(this.groupHasAccount)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.groupHasAccountService
            .add(this.groupHasAccount)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
