import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class GroupHasAccountService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'groupHasAccount/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'groupHasAccount/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'groupHasAccount/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'groupHasAccount/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'groupHasAccount/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'groupHasAccount/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'groupHasAccount/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'groupHasAccount/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'groupHasAccount/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'groupHasAccount/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'groupHasAccount/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'groupHasAccount/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'groupHasAccount/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'groupHasAccount/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'groupHasAccount/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'groupHasAccount/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'groupHasAccount/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'groupHasAccount/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'groupHasAccount/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'groupHasAccount/getForRelation';
        return this.http.post(url,vo);
    }
}
