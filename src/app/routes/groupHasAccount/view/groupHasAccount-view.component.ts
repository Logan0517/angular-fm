import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { GroupHasAccountService } from '../groupHasAccount.service';
import { GroupHasAccount } from '../groupHasAccount';
import { GroupHasAccountVO } from '../groupHasAccount.vo';
import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';


@Component({
    selector: 'app-grouphasaccount-view',
    templateUrl: './grouphasaccount-view.component.html',
})
export class GroupHasAccountViewComponent implements OnInit {

    loading = false;
    groupHasAccount: GroupHasAccount = new GroupHasAccount();
    groupHasAccountVO: GroupHasAccountVO = new GroupHasAccountVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private groupHasAccountService:GroupHasAccountService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: GroupHasAccount) => {
            this.groupHasAccountVO.groupId = param.groupId;
            this.groupHasAccountVO.accountId = param.accountId;
        });
        this.loading = true;
        this.groupHasAccountService
        .getByVo(this.groupHasAccountVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: GroupHasAccount) => {
            this.groupHasAccount = res;
        });
    }


}
