import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { GroupHasAccountViewComponent } from '../view/groupHasAccount-view.component';
import { GroupHasAccountEditComponent } from '../edit/groupHasAccount-edit.component';
import { GroupHasAccountService } from '../groupHasAccount.service';
import { GroupHasAccount } from '../groupHasAccount';
import { GroupHasAccountVO } from '../groupHasAccount.vo';
import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';

@Component({
    selector: 'app-grouphasaccount-list',
    templateUrl: './grouphasaccount-list.component.html',
    styles: [],
    providers: [GroupHasAccountEditComponent,GroupHasAccountViewComponent],
})
export class GroupHasAccountListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private groupHasAccountService: GroupHasAccountService,
    private groupService: GroupService,
    private accountService: AccountService,
  ) {}

  groupHasAccount: GroupHasAccount;
  groupHasAccountVO: GroupHasAccountVO;
  groupHasAccountVOs: GroupHasAccountVO[];
  groupHasAccounts: GroupHasAccount[];
  groups: Group[];
  groupVO: GroupVO;
  accounts: Account[];
  accountVO: AccountVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '分组', index: 'groupId' },
    { title: '用户', index: 'accountId' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: GroupHasAccount) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: GroupHasAccount) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: GroupHasAccount) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.groupHasAccountVO = new GroupHasAccountVO();
    this.groupHasAccountVO.page = 1;
    this.groupHasAccountVO.pageSize = 15;
    this.groupVO = new GroupVO();
    this.groupService.getAllByVO(this.groupVO).subscribe((res: Group[]) => {
      this.groups = res;
    });
    this.accountVO = new AccountVO();
    this.accountService.getAllByVO(this.accountVO).subscribe((res: Account[]) => {
      this.accounts = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      group: new FormControl({ name: 'group', value: '' }),/*分组*/
      account: new FormControl({ name: 'account', value: '' }),/*用户*/
    });
  }

  search() {
    this.loading = true;
    this.groupHasAccountService
      .getByPage(this.groupHasAccountVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.groupHasAccounts = res.datas;
        this.groupHasAccountVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.groupHasAccountVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: GroupHasAccount) {
    this.modal.createStatic(GroupHasAccountEditComponent, { groupHasAccountVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: GroupHasAccount) {
    this.loading = true;
    this.groupHasAccountService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: GroupHasAccount) {
    this.router.navigate(['/groupHasAccount/view/'+ item.groupId+'/'+item.accountId]);
  }

  removeMul() {
    this.loading = true;
    this.groupHasAccountVOs = [];
    this.selectedRows.forEach(selectRow => {
      const groupHasAccountTmpVO: GroupHasAccountVO = new GroupHasAccountVO();
      groupHasAccountTmpVO.groupId = selectRow.groupId;
      groupHasAccountTmpVO.accountId = selectRow.accountId;
      this.groupHasAccountVOs.push(groupHasAccountTmpVO);
    });
    this.groupHasAccountService
    .delByVOForLogicMul(this.groupHasAccountVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.groupHasAccountVOs = [];
    this.selectedRows.forEach(selectRow => {
    const groupHasAccountTmpVO: GroupHasAccountVO = new GroupHasAccountVO();
      groupHasAccountTmpVO.groupId = selectRow.groupId;
      groupHasAccountTmpVO.accountId = selectRow.accountId;
    this.groupHasAccountVOs.push(groupHasAccountTmpVO);
    });
    this.groupHasAccountService
    .updMul(this.groupHasAccountVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
