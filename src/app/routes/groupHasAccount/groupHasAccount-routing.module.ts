import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupHasAccountListComponent } from './list/groupHasAccount-list.component';
import { GroupHasAccountViewComponent } from './view/groupHasAccount-view.component';

const routes: Routes = [
    { path: 'list', component: GroupHasAccountListComponent },
    { path: 'view/:groupId/:accountId', component: GroupHasAccountViewComponent, data: { title: '用户分组多对多'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GroupHasAccountRoutingModule { }
