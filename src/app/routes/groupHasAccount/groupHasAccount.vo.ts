import { GroupVO } from '../group/group.vo';
import { AccountVO } from '../account/account.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 GroupHasAccount 用户分组多对多
*/
export class GroupHasAccountVO {
    groupId: any;/*分组*/
    accountId: any;/*用户*/
    groupVO: GroupVO;/*分组*/
    accountVO: AccountVO;/*用户*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}