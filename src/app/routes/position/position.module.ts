import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { PositionRoutingModule } from './position-routing.module';
import { PositionListComponent } from './list/position-list.component';
import { PositionViewComponent } from './view/position-view.component';
import { PositionEditComponent } from './edit/position-edit.component';

const COMPONENTS = [PositionListComponent,PositionViewComponent];
const COMPONENTS_NOROUNT = [PositionEditComponent];

@NgModule({
    imports: [SharedModule,PositionRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class PositionModule { }
