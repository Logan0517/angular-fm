import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { PositionService } from '../position.service';
import { Position } from '../position';
import { PositionVO } from '../position.vo';

@Component({
  selector: 'app-position-edit',
  templateUrl: './position-edit.component.html',
})
export class PositionEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private positionService: PositionService,
  ) {}

  form: FormGroup;
  loading = false;
  position: Position = new Position();
  positionVO: PositionVO = new PositionVO();

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.position = new Position();
    if(!this.positionVO){
        this.positionVO = new PositionVO();
    }
    if(this.positionVO.id){
      this.loading = false;
      this.positionService
        .getByVo(this.positionVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.position = res;
        });
    }
  }

  initForm() {
    this.form = new FormGroup({
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/*逻辑删除*/
        status: new FormControl({ name: 'status', value: '' }, [Validators.required]),/*状态*/
    });
  }
    submit() {
        if (this.position.id) {
            this.positionService
                .upd(this.position)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.positionService
            .add(this.position)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
