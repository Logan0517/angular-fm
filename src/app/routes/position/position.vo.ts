import { GroupVO } from '../group/group.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Position 职务
*/
export class PositionVO {
    id: any;/**/
    name: any;/*名称*/
    nameLike: any;/*名称模糊查询*/
    isDel: any;/*逻辑删除*/
    status: any;/*状态*/
    positionGroupsVO: GroupVO[];/* 用户分组*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}