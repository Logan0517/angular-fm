import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { PositionViewComponent } from '../view/position-view.component';
import { PositionEditComponent } from '../edit/position-edit.component';
import { PositionService } from '../position.service';
import { Position } from '../position';
import { PositionVO } from '../position.vo';

@Component({
    selector: 'app-position-list',
    templateUrl: './position-list.component.html',
    styles: [],
    providers: [PositionEditComponent,PositionViewComponent],
})
export class PositionListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private positionService: PositionService,
  ) {}

  position: Position;
  positionVO: PositionVO;
  positionVOs: PositionVO[];
  positions: Position[];
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '名称', index: 'name' },
    { title: '逻辑删除', index: 'isDel' },
    { title: '状态', index: 'status' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Position) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Position) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Position) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.positionVO = new PositionVO();
    this.positionVO.page = 1;
    this.positionVO.pageSize = 15;
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*名称模糊查询*/
    });
  }

  search() {
    this.loading = true;
    this.positionService
      .getByPage(this.positionVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.positions = res.datas;
        this.positionVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.positionVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Position) {
    this.modal.createStatic(PositionEditComponent, { positionVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Position) {
    this.loading = true;
    this.positionService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Position) {
    this.router.navigate(['/position/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.positionVOs = [];
    this.selectedRows.forEach(selectRow => {
      const positionTmpVO: PositionVO = new PositionVO();
      positionTmpVO.id = selectRow.id;
      this.positionVOs.push(positionTmpVO);
    });
    this.positionService
    .delByVOForLogicMul(this.positionVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.positionVOs = [];
    this.selectedRows.forEach(selectRow => {
    const positionTmpVO: PositionVO = new PositionVO();
      positionTmpVO.id = selectRow.id;
    this.positionVOs.push(positionTmpVO);
    });
    this.positionService
    .updMul(this.positionVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
