import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PositionListComponent } from './list/position-list.component';
import { PositionViewComponent } from './view/position-view.component';

const routes: Routes = [
    { path: 'list', component: PositionListComponent },
    { path: 'view/:id', component: PositionViewComponent, data: { title: '职务'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PositionRoutingModule { }
