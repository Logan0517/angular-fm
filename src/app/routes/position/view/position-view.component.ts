import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { PositionService } from '../position.service';
import { Position } from '../position';
import { PositionVO } from '../position.vo';

import { GroupService } from '../../group/group.service';
import { Group } from '../../group/group';
import { GroupVO } from '../../group/group.vo';

@Component({
    selector: 'app-position-view',
    templateUrl: './position-view.component.html',
})
export class PositionViewComponent implements OnInit {

    loading = false;
    position: Position = new Position();
    positionVO: PositionVO = new PositionVO();
    /** 用户分组 */
    positionGroups: Group[] = [];
    positionGroupsVO: GroupVO = new GroupVO();
    positionGroupsLoading = false;

    /** 用户分组 */
    positionGroupsColumns: STColumn[] = [
        { title: '组名', index: 'name', default: '-'},
        { title: '逻辑删除', index: 'isDel', default: '-'},
        { title: '标识', index: 'id', default: '-'},
        { title: '角色', index: 'roleId', default: '-'},
        { title: '职位', index: 'positionId', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private positionService:PositionService,
        private groupService:GroupService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initPositionGroups();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Position) => {
            this.positionVO.id = param.id;
        });
        this.loading = true;
        this.positionService
        .getByVo(this.positionVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Position) => {
            this.position = res;
        });
    }

    /** 用户分组 */
    initPositionGroups() {
        this.positionGroupsLoading = true;
        this.positionGroupsVO.positionVO= this.positionVO;
        this.groupService
        .getAllByVO(this.positionGroupsVO)
        .pipe(finalize(() => (this.positionGroupsLoading = false)))
        .subscribe((res: Group[]) => {
            this.positionGroups = res;
        });
    }


}
