import { Group } from '../group/group';

/*
* Created by jiangchen
* Automated Build
* 实体 Position 职务
*/
export class Position {
    id: any;/**/
    name: any;/*名称*/
    isDel: any;/*逻辑删除*/
    status: any;/*状态*/
    positionGroups: Group[];/* 用户分组*/
}