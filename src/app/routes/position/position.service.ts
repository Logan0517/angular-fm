import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class PositionService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'position/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'position/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'position/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'position/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'position/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'position/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'position/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'position/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'position/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'position/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'position/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'position/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'position/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'position/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'position/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'position/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'position/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'position/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'position/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'position/getForRelation';
        return this.http.post(url,vo);
    }
}
