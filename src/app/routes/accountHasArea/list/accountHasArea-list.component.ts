import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { AccountHasAreaViewComponent } from '../view/accountHasArea-view.component';
import { AccountHasAreaEditComponent } from '../edit/accountHasArea-edit.component';
import { AccountHasAreaService } from '../accountHasArea.service';
import { AccountHasArea } from '../accountHasArea';
import { AccountHasAreaVO } from '../accountHasArea.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';

@Component({
    selector: 'app-accounthasarea-list',
    templateUrl: './accounthasarea-list.component.html',
    styles: [],
    providers: [AccountHasAreaEditComponent,AccountHasAreaViewComponent],
})
export class AccountHasAreaListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private accountHasAreaService: AccountHasAreaService,
    private areaService: AreaService,
    private accountService: AccountService,
  ) {}

  accountHasArea: AccountHasArea;
  accountHasAreaVO: AccountHasAreaVO;
  accountHasAreaVOs: AccountHasAreaVO[];
  accountHasAreas: AccountHasArea[];
  areas: Area[];
  areaVO: AreaVO;
  accounts: Account[];
  accountVO: AccountVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'areaId' },
    { title: '', index: 'accountId' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: AccountHasArea) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: AccountHasArea) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: AccountHasArea) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.accountHasAreaVO = new AccountHasAreaVO();
    this.accountHasAreaVO.page = 1;
    this.accountHasAreaVO.pageSize = 15;
    this.areaVO = new AreaVO();
    this.areaService.getAllByVO(this.areaVO).subscribe((res: Area[]) => {
      this.areas = res;
    });
    this.accountVO = new AccountVO();
    this.accountService.getAllByVO(this.accountVO).subscribe((res: Account[]) => {
      this.accounts = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      area: new FormControl({ name: 'area', value: '' }),/**/
      account: new FormControl({ name: 'account', value: '' }),/**/
    });
  }

  search() {
    this.loading = true;
    this.accountHasAreaService
      .getByPage(this.accountHasAreaVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.accountHasAreas = res.datas;
        this.accountHasAreaVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.accountHasAreaVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: AccountHasArea) {
    this.modal.createStatic(AccountHasAreaEditComponent, { accountHasAreaVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: AccountHasArea) {
    this.loading = true;
    this.accountHasAreaService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: AccountHasArea) {
    this.router.navigate(['/accountHasArea/view/'+ item.areaId+'/'+item.accountId]);
  }

  removeMul() {
    this.loading = true;
    this.accountHasAreaVOs = [];
    this.selectedRows.forEach(selectRow => {
      const accountHasAreaTmpVO: AccountHasAreaVO = new AccountHasAreaVO();
      accountHasAreaTmpVO.areaId = selectRow.areaId;
      accountHasAreaTmpVO.accountId = selectRow.accountId;
      this.accountHasAreaVOs.push(accountHasAreaTmpVO);
    });
    this.accountHasAreaService
    .delByVOForLogicMul(this.accountHasAreaVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.accountHasAreaVOs = [];
    this.selectedRows.forEach(selectRow => {
    const accountHasAreaTmpVO: AccountHasAreaVO = new AccountHasAreaVO();
      accountHasAreaTmpVO.areaId = selectRow.areaId;
      accountHasAreaTmpVO.accountId = selectRow.accountId;
    this.accountHasAreaVOs.push(accountHasAreaTmpVO);
    });
    this.accountHasAreaService
    .updMul(this.accountHasAreaVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
