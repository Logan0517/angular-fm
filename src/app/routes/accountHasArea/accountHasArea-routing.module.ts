import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountHasAreaListComponent } from './list/accountHasArea-list.component';
import { AccountHasAreaViewComponent } from './view/accountHasArea-view.component';

const routes: Routes = [
    { path: 'list', component: AccountHasAreaListComponent },
    { path: 'view/:areaId/:accountId', component: AccountHasAreaViewComponent, data: { title: ''}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AccountHasAreaRoutingModule { }
