import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { AccountHasAreaService } from '../accountHasArea.service';
import { AccountHasArea } from '../accountHasArea';
import { AccountHasAreaVO } from '../accountHasArea.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';

@Component({
  selector: 'app-accounthasarea-edit',
  templateUrl: './accounthasarea-edit.component.html',
})
export class AccountHasAreaEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private accountHasAreaService: AccountHasAreaService,
    private areaService: AreaService,
    private accountService: AccountService,
  ) {}

  form: FormGroup;
  loading = false;
  accountHasArea: AccountHasArea = new AccountHasArea();
  accountHasAreaVO: AccountHasAreaVO = new AccountHasAreaVO();
  areas: Area[];
  areaVO: AreaVO;
  accounts: Account[];
  accountVO: AccountVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.accountHasArea = new AccountHasArea();
    if(!this.accountHasAreaVO){
        this.accountHasAreaVO = new AccountHasAreaVO();
    }
    if(this.accountHasAreaVO.areaId && this.accountHasAreaVO.accountId){
      this.loading = false;
      this.accountHasAreaService
        .getByVo(this.accountHasAreaVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.accountHasArea = res;
        });
    }
    this.areaVO = new AreaVO();
    this.areaService.getAllByVO(this.areaVO).subscribe((res: Area[]) => {
      this.areas = res;
    });
    this.accountVO = new AccountVO();
    this.accountService.getAllByVO(this.accountVO).subscribe((res: Account[]) => {
      this.accounts = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        area: new FormControl({ name: 'area', value: '' }, Validators.required),/**/
        account: new FormControl({ name: 'account', value: '' }, Validators.required),/**/
    });
  }
    submit() {
        if (this.accountHasArea.areaId && this.accountHasArea.accountId) {
            this.accountHasAreaService
                .upd(this.accountHasArea)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.accountHasAreaService
            .add(this.accountHasArea)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
