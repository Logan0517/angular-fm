import { Area } from '../area/area';
import { Account } from '../account/account';

/*
* Created by jiangchen
* Automated Build
* 实体 AccountHasArea 
*/
export class AccountHasArea {
    areaId: any;/**/
    accountId: any;/**/
    area: Area;/**/
    account: Account;/**/
}