import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { AccountHasAreaRoutingModule } from './accountHasArea-routing.module';
import { AccountHasAreaListComponent } from './list/accountHasArea-list.component';
import { AccountHasAreaViewComponent } from './view/accountHasArea-view.component';
import { AccountHasAreaEditComponent } from './edit/accountHasArea-edit.component';

const COMPONENTS = [AccountHasAreaListComponent,AccountHasAreaViewComponent];
const COMPONENTS_NOROUNT = [AccountHasAreaEditComponent];

@NgModule({
    imports: [SharedModule,AccountHasAreaRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class AccountHasAreaModule { }
