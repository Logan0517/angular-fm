import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { AccountHasAreaService } from '../accountHasArea.service';
import { AccountHasArea } from '../accountHasArea';
import { AccountHasAreaVO } from '../accountHasArea.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';


@Component({
    selector: 'app-accounthasarea-view',
    templateUrl: './accounthasarea-view.component.html',
})
export class AccountHasAreaViewComponent implements OnInit {

    loading = false;
    accountHasArea: AccountHasArea = new AccountHasArea();
    accountHasAreaVO: AccountHasAreaVO = new AccountHasAreaVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private accountHasAreaService:AccountHasAreaService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: AccountHasArea) => {
            this.accountHasAreaVO.areaId = param.areaId;
            this.accountHasAreaVO.accountId = param.accountId;
        });
        this.loading = true;
        this.accountHasAreaService
        .getByVo(this.accountHasAreaVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: AccountHasArea) => {
            this.accountHasArea = res;
        });
    }


}
