import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class AccountHasAreaService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'accountHasArea/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'accountHasArea/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'accountHasArea/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'accountHasArea/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'accountHasArea/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'accountHasArea/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'accountHasArea/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'accountHasArea/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'accountHasArea/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'accountHasArea/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'accountHasArea/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'accountHasArea/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'accountHasArea/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'accountHasArea/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'accountHasArea/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'accountHasArea/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'accountHasArea/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'accountHasArea/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'accountHasArea/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'accountHasArea/getForRelation';
        return this.http.post(url,vo);
    }
}
