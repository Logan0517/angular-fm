import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { DeviceViewComponent } from '../view/device-view.component';
import { DeviceEditComponent } from '../edit/device-edit.component';
import { DeviceService } from '../device.service';
import { Device } from '../device';
import { DeviceVO } from '../device.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';

@Component({
    selector: 'app-device-list',
    templateUrl: './device-list.component.html',
    styles: [],
    providers: [DeviceEditComponent,DeviceViewComponent],
})
export class DeviceListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private deviceService: DeviceService,
    private areaService: AreaService,
  ) {}

  device: Device;
  deviceVO: DeviceVO;
  deviceVOs: DeviceVO[];
  devices: Device[];
  areas: Area[];
  areaVO: AreaVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '经度', index: 'gpsLng' },
    { title: '系统code', index: 'code' },
    { title: '分组数量', index: 'groupCount' },
    { title: '设备编码', index: 'deviceCode' },
    { title: '设备名称', index: 'name' },
    { title: '', index: 'isDel' },
    { title: '探头数量', index: 'probeCount' },
    { title: '纬度', index: 'gpsLat' },
    { title: '设备类型', index: 'type' },
    { title: '所在区域', index: 'areaId' },
    { title: '状态', index: 'status' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Device) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Device) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Device) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.deviceVO = new DeviceVO();
    this.deviceVO.page = 1;
    this.deviceVO.pageSize = 15;
    this.areaVO = new AreaVO();
    this.areaService.getAllByVO(this.areaVO).subscribe((res: Area[]) => {
      this.areas = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*设备名称模糊查询*/
      area: new FormControl({ name: 'area', value: '' }),/*所在区域*/
    });
  }

  search() {
    this.loading = true;
    this.deviceService
      .getByPage(this.deviceVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.devices = res.datas;
        this.deviceVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.deviceVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Device) {
    this.modal.createStatic(DeviceEditComponent, { deviceVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Device) {
    this.loading = true;
    this.deviceService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Device) {
    this.router.navigate(['/device/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.deviceVOs = [];
    this.selectedRows.forEach(selectRow => {
      const deviceTmpVO: DeviceVO = new DeviceVO();
      deviceTmpVO.id = selectRow.id;
      this.deviceVOs.push(deviceTmpVO);
    });
    this.deviceService
    .delByVOForLogicMul(this.deviceVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.deviceVOs = [];
    this.selectedRows.forEach(selectRow => {
    const deviceTmpVO: DeviceVO = new DeviceVO();
      deviceTmpVO.id = selectRow.id;
    this.deviceVOs.push(deviceTmpVO);
    });
    this.deviceService
    .updMul(this.deviceVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
