import { AreaVO } from '../area/area.vo';
import { ProbeGroupVO } from '../probeGroup/probeGroup.vo';
import { ProbeVO } from '../probe/probe.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Device 设备
*/
export class DeviceVO {
    id: any;/**/
    gpsLng: any;/*经度*/
    code: any;/*系统code*/
    groupCount: any;/*分组数量*/
    deviceCode: any;/*设备编码*/
    name: any;/*设备名称*/
    nameLike: any;/*设备名称模糊查询*/
    isDel: any;/**/
    probeCount: any;/*探头数量*/
    gpsLat: any;/*纬度*/
    type: any;/*设备类型*/
    areaId: any;/*所在区域*/
    status: any;/*状态*/
    areaVO: AreaVO;/*所在区域*/
    deviceProbeGroupsVO: ProbeGroupVO[];/* 探头分组*/
    deviceProbesVO: ProbeVO[];/* 探头*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}