import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeviceListComponent } from './list/device-list.component';
import { DeviceViewComponent } from './view/device-view.component';

const routes: Routes = [
    { path: 'list', component: DeviceListComponent },
    { path: 'view/:id', component: DeviceViewComponent, data: { title: '设备'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DeviceRoutingModule { }
