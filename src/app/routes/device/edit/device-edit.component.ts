import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { DeviceService } from '../device.service';
import { Device } from '../device';
import { DeviceVO } from '../device.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';

@Component({
  selector: 'app-device-edit',
  templateUrl: './device-edit.component.html',
})
export class DeviceEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private deviceService: DeviceService,
    private areaService: AreaService,
  ) {}

  form: FormGroup;
  loading = false;
  device: Device = new Device();
  deviceVO: DeviceVO = new DeviceVO();
  areas: Area[];
  areaVO: AreaVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.device = new Device();
    if(!this.deviceVO){
        this.deviceVO = new DeviceVO();
    }
    if(this.deviceVO.id){
      this.loading = false;
      this.deviceService
        .getByVo(this.deviceVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.device = res;
        });
    }
    this.areaVO = new AreaVO();
    this.areaService.getAllByVO(this.areaVO).subscribe((res: Area[]) => {
      this.areas = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        area: new FormControl({ name: 'area', value: '' }, Validators.required),/*所在区域*/
        gpsLng: new FormControl({ name: 'gpsLng', value: '' }, [Validators.required]),/*经度*/
        code: new FormControl({ name: 'code', value: '' }, [Validators.required]),/*系统code*/
        groupCount: new FormControl({ name: 'groupCount', value: '' }, [Validators.required]),/*分组数量*/
        deviceCode: new FormControl({ name: 'deviceCode', value: '' }, [Validators.required]),/*设备编码*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*设备名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        probeCount: new FormControl({ name: 'probeCount', value: '' }, [Validators.required]),/*探头数量*/
        gpsLat: new FormControl({ name: 'gpsLat', value: '' }, [Validators.required]),/*纬度*/
        type: new FormControl({ name: 'type', value: '' }, [Validators.required]),/*设备类型*/
        areaId: new FormControl({ name: 'areaId', value: '' }, [Validators.required]),/*所在区域*/
        status: new FormControl({ name: 'status', value: '' }, [Validators.required]),/*状态*/
    });
  }
    submit() {
        if (this.device.id) {
            this.deviceService
                .upd(this.device)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.deviceService
            .add(this.device)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
