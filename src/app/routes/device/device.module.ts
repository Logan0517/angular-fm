import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { DeviceRoutingModule } from './device-routing.module';
import { DeviceListComponent } from './list/device-list.component';
import { DeviceViewComponent } from './view/device-view.component';
import { DeviceEditComponent } from './edit/device-edit.component';

const COMPONENTS = [DeviceListComponent,DeviceViewComponent];
const COMPONENTS_NOROUNT = [DeviceEditComponent];

@NgModule({
    imports: [SharedModule,DeviceRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class DeviceModule { }
