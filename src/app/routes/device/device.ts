import { Area } from '../area/area';
import { ProbeGroup } from '../probeGroup/probeGroup';
import { Probe } from '../probe/probe';

/*
* Created by jiangchen
* Automated Build
* 实体 Device 设备
*/
export class Device {
    id: any;/**/
    gpsLng: any;/*经度*/
    code: any;/*系统code*/
    groupCount: any;/*分组数量*/
    deviceCode: any;/*设备编码*/
    name: any;/*设备名称*/
    isDel: any;/**/
    probeCount: any;/*探头数量*/
    gpsLat: any;/*纬度*/
    type: any;/*设备类型*/
    areaId: any;/*所在区域*/
    status: any;/*状态*/
    area: Area;/*所在区域*/
    deviceProbeGroups: ProbeGroup[];/* 探头分组*/
    deviceProbes: Probe[];/* 探头*/
}