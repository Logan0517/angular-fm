import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class DeviceService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'device/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'device/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'device/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'device/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'device/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'device/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'device/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'device/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'device/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'device/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'device/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'device/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'device/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'device/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'device/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'device/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'device/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'device/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'device/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'device/getForRelation';
        return this.http.post(url,vo);
    }
}
