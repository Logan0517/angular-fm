import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { DeviceService } from '../device.service';
import { Device } from '../device';
import { DeviceVO } from '../device.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';

import { ProbeGroupService } from '../../probeGroup/probeGroup.service';
import { ProbeGroup } from '../../probeGroup/probeGroup';
import { ProbeGroupVO } from '../../probeGroup/probeGroup.vo';
import { ProbeService } from '../../probe/probe.service';
import { Probe } from '../../probe/probe';
import { ProbeVO } from '../../probe/probe.vo';

@Component({
    selector: 'app-device-view',
    templateUrl: './device-view.component.html',
})
export class DeviceViewComponent implements OnInit {

    loading = false;
    device: Device = new Device();
    deviceVO: DeviceVO = new DeviceVO();
    /** 探头分组 */
    deviceProbeGroups: ProbeGroup[] = [];
    deviceProbeGroupsVO: ProbeGroupVO = new ProbeGroupVO();
    deviceProbeGroupsLoading = false;
    /** 探头 */
    deviceProbes: Probe[] = [];
    deviceProbesVO: ProbeVO = new ProbeVO();
    deviceProbesLoading = false;

    /** 探头分组 */
    deviceProbeGroupsColumns: STColumn[] = [
        { title: '编码', index: 'code', default: '-'},
        { title: '设备编码', index: 'deviceCode', default: '-'},
        { title: '排序编号 从1开始', index: 'sortNum', default: '-'},
        { title: '分组名称', index: 'name', default: '-'},
        { title: '', index: 'isDel', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '设备业务编码', index: 'deviceBusCode', default: '-'},
    ];
    /** 探头 */
    deviceProbesColumns: STColumn[] = [
        { title: '编码', index: 'code', default: '-'},
        { title: '分组', index: 'groupCode', default: '-'},
        { title: '分组序号 0代表没有分组', index: 'groupNum', default: '-'},
        { title: '设备编码', index: 'deviceCode', default: '-'},
        { title: '排序编号 从1开始', index: 'sortNum', default: '-'},
        { title: '名称', index: 'name', default: '-'},
        { title: '', index: 'isDel', default: '-'},
        { title: '', index: 'id', default: '-'},
        { title: '设备业务编码', index: 'deviceBusCode', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private deviceService:DeviceService,
        private probeGroupService:ProbeGroupService,
        private probeService:ProbeService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initDeviceProbeGroups();
        this.initDeviceProbes();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Device) => {
            this.deviceVO.id = param.id;
        });
        this.loading = true;
        this.deviceService
        .getByVo(this.deviceVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Device) => {
            this.device = res;
        });
    }

    /** 探头分组 */
    initDeviceProbeGroups() {
        this.deviceProbeGroupsLoading = true;
        this.deviceProbeGroupsVO.deviceVO= this.deviceVO;
        this.probeGroupService
        .getAllByVO(this.deviceProbeGroupsVO)
        .pipe(finalize(() => (this.deviceProbeGroupsLoading = false)))
        .subscribe((res: ProbeGroup[]) => {
            this.deviceProbeGroups = res;
        });
    }

    /** 探头 */
    initDeviceProbes() {
        this.deviceProbesLoading = true;
        this.deviceProbesVO.deviceVO= this.deviceVO;
        this.probeService
        .getAllByVO(this.deviceProbesVO)
        .pipe(finalize(() => (this.deviceProbesLoading = false)))
        .subscribe((res: Probe[]) => {
            this.deviceProbes = res;
        });
    }


}
