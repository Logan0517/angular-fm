import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { SupplierViewComponent } from '../view/supplier-view.component';
import { SupplierEditComponent } from '../edit/supplier-edit.component';
import { SupplierService } from '../supplier.service';
import { Supplier } from '../supplier';
import { SupplierVO } from '../supplier.vo';

@Component({
    selector: 'app-supplier-list',
    templateUrl: './supplier-list.component.html',
    styles: [],
    providers: [SupplierEditComponent,SupplierViewComponent],
})
export class SupplierListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private supplierService: SupplierService,
  ) {}

  supplier: Supplier;
  supplierVO: SupplierVO;
  supplierVOs: SupplierVO[];
  suppliers: Supplier[];
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '编码', index: 'code' },
    { title: '创建时间', index: 'createTime' },
    { title: '名称', index: 'name' },
    { title: '', index: 'isDel' },
    { title: '业务编码', index: 'businessCode' },
    { title: '状态', index: 'status' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Supplier) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Supplier) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Supplier) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.supplierVO = new SupplierVO();
    this.supplierVO.page = 1;
    this.supplierVO.pageSize = 15;
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
       createTimeEndBegin: new FormControl({ name: 'createTimeEndBegin', value: '' }),/*创建时间*/
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*名称模糊查询*/
    });
  }

  search() {
    this.loading = true;
    this.supplierService
      .getByPage(this.supplierVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.suppliers = res.datas;
        this.supplierVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.supplierVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Supplier) {
    this.modal.createStatic(SupplierEditComponent, { supplierVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Supplier) {
    this.loading = true;
    this.supplierService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Supplier) {
    this.router.navigate(['/supplier/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.supplierVOs = [];
    this.selectedRows.forEach(selectRow => {
      const supplierTmpVO: SupplierVO = new SupplierVO();
      supplierTmpVO.id = selectRow.id;
      this.supplierVOs.push(supplierTmpVO);
    });
    this.supplierService
    .delByVOForLogicMul(this.supplierVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.supplierVOs = [];
    this.selectedRows.forEach(selectRow => {
    const supplierTmpVO: SupplierVO = new SupplierVO();
      supplierTmpVO.id = selectRow.id;
    this.supplierVOs.push(supplierTmpVO);
    });
    this.supplierService
    .updMul(this.supplierVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
