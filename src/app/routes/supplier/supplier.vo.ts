
/*
* Created by jiangchen
* Automated Build
* 实体 Supplier 供应商
*/
export class SupplierVO {
    id: any;/**/
    code: any;/*编码*/
    createTime: any;/*创建时间*/
    createTimeEnd: any;/*创建时间开始*/
    createTimeBegin: any;/*创建时间结束*/
    name: any;/*名称*/
    nameLike: any;/*名称模糊查询*/
    isDel: any;/**/
    businessCode: any;/*业务编码*/
    status: any;/*状态*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}