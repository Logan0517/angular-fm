import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { SupplierService } from '../supplier.service';
import { Supplier } from '../supplier';
import { SupplierVO } from '../supplier.vo';

@Component({
  selector: 'app-supplier-edit',
  templateUrl: './supplier-edit.component.html',
})
export class SupplierEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private supplierService: SupplierService,
  ) {}

  form: FormGroup;
  loading = false;
  supplier: Supplier = new Supplier();
  supplierVO: SupplierVO = new SupplierVO();

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.supplier = new Supplier();
    if(!this.supplierVO){
        this.supplierVO = new SupplierVO();
    }
    if(this.supplierVO.id){
      this.loading = false;
      this.supplierService
        .getByVo(this.supplierVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.supplier = res;
        });
    }
  }

  initForm() {
    this.form = new FormGroup({
        code: new FormControl({ name: 'code', value: '' }, [Validators.required]),/*编码*/
        createTime: new FormControl({ name: 'createTime', value: '' }, [Validators.required]),/*创建时间*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*名称*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        businessCode: new FormControl({ name: 'businessCode', value: '' }, [Validators.required]),/*业务编码*/
        status: new FormControl({ name: 'status', value: '' }, [Validators.required]),/*状态*/
    });
  }
    submit() {
        if (this.supplier.id) {
            this.supplierService
                .upd(this.supplier)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.supplierService
            .add(this.supplier)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
