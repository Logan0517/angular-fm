import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { SupplierService } from '../supplier.service';
import { Supplier } from '../supplier';
import { SupplierVO } from '../supplier.vo';


@Component({
    selector: 'app-supplier-view',
    templateUrl: './supplier-view.component.html',
})
export class SupplierViewComponent implements OnInit {

    loading = false;
    supplier: Supplier = new Supplier();
    supplierVO: SupplierVO = new SupplierVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private supplierService:SupplierService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Supplier) => {
            this.supplierVO.id = param.id;
        });
        this.loading = true;
        this.supplierService
        .getByVo(this.supplierVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Supplier) => {
            this.supplier = res;
        });
    }


}
