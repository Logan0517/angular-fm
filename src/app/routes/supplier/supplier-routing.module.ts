import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierListComponent } from './list/supplier-list.component';
import { SupplierViewComponent } from './view/supplier-view.component';

const routes: Routes = [
    { path: 'list', component: SupplierListComponent },
    { path: 'view/:id', component: SupplierViewComponent, data: { title: '供应商'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SupplierRoutingModule { }
