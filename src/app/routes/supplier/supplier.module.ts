import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { SupplierRoutingModule } from './supplier-routing.module';
import { SupplierListComponent } from './list/supplier-list.component';
import { SupplierViewComponent } from './view/supplier-view.component';
import { SupplierEditComponent } from './edit/supplier-edit.component';

const COMPONENTS = [SupplierListComponent,SupplierViewComponent];
const COMPONENTS_NOROUNT = [SupplierEditComponent];

@NgModule({
    imports: [SharedModule,SupplierRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class SupplierModule { }
