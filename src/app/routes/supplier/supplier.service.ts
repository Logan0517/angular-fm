import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class SupplierService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'supplier/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'supplier/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'supplier/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'supplier/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'supplier/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'supplier/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'supplier/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'supplier/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'supplier/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'supplier/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'supplier/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'supplier/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'supplier/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'supplier/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'supplier/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'supplier/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'supplier/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'supplier/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'supplier/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'supplier/getForRelation';
        return this.http.post(url,vo);
    }
}
