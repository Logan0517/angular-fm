import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class ManyTableService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'manyTable/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'manyTable/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'manyTable/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'manyTable/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'manyTable/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'manyTable/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'manyTable/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'manyTable/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'manyTable/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'manyTable/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'manyTable/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'manyTable/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'manyTable/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'manyTable/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'manyTable/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'manyTable/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'manyTable/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'manyTable/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'manyTable/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'manyTable/getForRelation';
        return this.http.post(url,vo);
    }
}
