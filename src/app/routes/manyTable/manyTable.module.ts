import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ManyTableRoutingModule } from './manyTable-routing.module';
import { ManyTableListComponent } from './list/manyTable-list.component';
import { ManyTableViewComponent } from './view/manyTable-view.component';
import { ManyTableEditComponent } from './edit/manyTable-edit.component';

const COMPONENTS = [ManyTableListComponent,ManyTableViewComponent];
const COMPONENTS_NOROUNT = [ManyTableEditComponent];

@NgModule({
    imports: [SharedModule,ManyTableRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class ManyTableModule { }
