import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { ManyTableService } from '../manyTable.service';
import { ManyTable } from '../manyTable';
import { ManyTableVO } from '../manyTable.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';

@Component({
  selector: 'app-manytable-edit',
  templateUrl: './manytable-edit.component.html',
})
export class ManyTableEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private manyTableService: ManyTableService,
    private areaService: AreaService,
    private accountService: AccountService,
  ) {}

  form: FormGroup;
  loading = false;
  manyTable: ManyTable = new ManyTable();
  manyTableVO: ManyTableVO = new ManyTableVO();
  areas: Area[];
  areaVO: AreaVO;
  accounts: Account[];
  accountVO: AccountVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.manyTable = new ManyTable();
    if(!this.manyTableVO){
        this.manyTableVO = new ManyTableVO();
    }
    if(this.manyTableVO.id){
      this.loading = false;
      this.manyTableService
        .getByVo(this.manyTableVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.manyTable = res;
        });
    }
    this.areaVO = new AreaVO();
    this.areaService.getAllByVO(this.areaVO).subscribe((res: Area[]) => {
      this.areas = res;
    });
    this.accountVO = new AccountVO();
    this.accountService.getAllByVO(this.accountVO).subscribe((res: Account[]) => {
      this.accounts = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        area: new FormControl({ name: 'area', value: '' }, Validators.required),/**/
        account: new FormControl({ name: 'account', value: '' }, Validators.required),/**/
        areaId: new FormControl({ name: 'areaId', value: '' }, [Validators.required]),/**/
        accountId: new FormControl({ name: 'accountId', value: '' }, [Validators.required]),/**/
    });
  }
    submit() {
        if (this.manyTable.id) {
            this.manyTableService
                .upd(this.manyTable)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.manyTableService
            .add(this.manyTable)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
