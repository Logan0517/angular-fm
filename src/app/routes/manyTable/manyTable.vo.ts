import { AreaVO } from '../area/area.vo';
import { AccountVO } from '../account/account.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 ManyTable 测试多对多表
*/
export class ManyTableVO {
    id: any;/**/
    areaId: any;/**/
    accountId: any;/**/
    areaVO: AreaVO;/**/
    accountVO: AccountVO;/**/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}