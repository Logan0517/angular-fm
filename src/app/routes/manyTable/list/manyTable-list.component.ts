import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { ManyTableViewComponent } from '../view/manyTable-view.component';
import { ManyTableEditComponent } from '../edit/manyTable-edit.component';
import { ManyTableService } from '../manyTable.service';
import { ManyTable } from '../manyTable';
import { ManyTableVO } from '../manyTable.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';

@Component({
    selector: 'app-manytable-list',
    templateUrl: './manytable-list.component.html',
    styles: [],
    providers: [ManyTableEditComponent,ManyTableViewComponent],
})
export class ManyTableListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private manyTableService: ManyTableService,
    private areaService: AreaService,
    private accountService: AccountService,
  ) {}

  manyTable: ManyTable;
  manyTableVO: ManyTableVO;
  manyTableVOs: ManyTableVO[];
  manyTables: ManyTable[];
  areas: Area[];
  areaVO: AreaVO;
  accounts: Account[];
  accountVO: AccountVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '', index: 'areaId' },
    { title: '', index: 'accountId' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: ManyTable) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: ManyTable) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: ManyTable) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.manyTableVO = new ManyTableVO();
    this.manyTableVO.page = 1;
    this.manyTableVO.pageSize = 15;
    this.areaVO = new AreaVO();
    this.areaService.getAllByVO(this.areaVO).subscribe((res: Area[]) => {
      this.areas = res;
    });
    this.accountVO = new AccountVO();
    this.accountService.getAllByVO(this.accountVO).subscribe((res: Account[]) => {
      this.accounts = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      area: new FormControl({ name: 'area', value: '' }),/**/
      account: new FormControl({ name: 'account', value: '' }),/**/
    });
  }

  search() {
    this.loading = true;
    this.manyTableService
      .getByPage(this.manyTableVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.manyTables = res.datas;
        this.manyTableVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.manyTableVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: ManyTable) {
    this.modal.createStatic(ManyTableEditComponent, { manyTableVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: ManyTable) {
    this.loading = true;
    this.manyTableService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: ManyTable) {
    this.router.navigate(['/manyTable/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.manyTableVOs = [];
    this.selectedRows.forEach(selectRow => {
      const manyTableTmpVO: ManyTableVO = new ManyTableVO();
      manyTableTmpVO.id = selectRow.id;
      this.manyTableVOs.push(manyTableTmpVO);
    });
    this.manyTableService
    .delByVOForLogicMul(this.manyTableVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.manyTableVOs = [];
    this.selectedRows.forEach(selectRow => {
    const manyTableTmpVO: ManyTableVO = new ManyTableVO();
      manyTableTmpVO.id = selectRow.id;
    this.manyTableVOs.push(manyTableTmpVO);
    });
    this.manyTableService
    .updMul(this.manyTableVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
