import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManyTableListComponent } from './list/manyTable-list.component';
import { ManyTableViewComponent } from './view/manyTable-view.component';

const routes: Routes = [
    { path: 'list', component: ManyTableListComponent },
    { path: 'view/:id', component: ManyTableViewComponent, data: { title: '测试多对多表'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManyTableRoutingModule { }
