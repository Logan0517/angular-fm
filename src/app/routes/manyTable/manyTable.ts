import { Area } from '../area/area';
import { Account } from '../account/account';

/*
* Created by jiangchen
* Automated Build
* 实体 ManyTable 测试多对多表
*/
export class ManyTable {
    id: any;/**/
    areaId: any;/**/
    accountId: any;/**/
    area: Area;/**/
    account: Account;/**/
}