import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { ManyTableService } from '../manyTable.service';
import { ManyTable } from '../manyTable';
import { ManyTableVO } from '../manyTable.vo';
import { AreaService } from '../../area/area.service';
import { Area } from '../../area/area';
import { AreaVO } from '../../area/area.vo';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account';
import { AccountVO } from '../../account/account.vo';


@Component({
    selector: 'app-manytable-view',
    templateUrl: './manytable-view.component.html',
})
export class ManyTableViewComponent implements OnInit {

    loading = false;
    manyTable: ManyTable = new ManyTable();
    manyTableVO: ManyTableVO = new ManyTableVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private manyTableService:ManyTableService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: ManyTable) => {
            this.manyTableVO.id = param.id;
        });
        this.loading = true;
        this.manyTableService
        .getByVo(this.manyTableVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: ManyTable) => {
            this.manyTable = res;
        });
    }


}
