import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { SystmSettingService } from '../systmSetting.service';
import { SystmSetting } from '../systmSetting';
import { SystmSettingVO } from '../systmSetting.vo';

@Component({
  selector: 'app-systmsetting-edit',
  templateUrl: './systmsetting-edit.component.html',
})
export class SystmSettingEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private systmSettingService: SystmSettingService,
  ) {}

  form: FormGroup;
  loading = false;
  systmSetting: SystmSetting = new SystmSetting();
  systmSettingVO: SystmSettingVO = new SystmSettingVO();

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.systmSetting = new SystmSetting();
    if(!this.systmSettingVO){
        this.systmSettingVO = new SystmSettingVO();
    }
    if(this.systmSettingVO.id){
      this.loading = false;
      this.systmSettingService
        .getByVo(this.systmSettingVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.systmSetting = res;
        });
    }
  }

  initForm() {
    this.form = new FormGroup({
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/**/
        temperatureWarnning: new FormControl({ name: 'temperatureWarnning', value: '' }, [Validators.required]),/*报警温度*/
    });
  }
    submit() {
        if (this.systmSetting.id) {
            this.systmSettingService
                .upd(this.systmSetting)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.systmSettingService
            .add(this.systmSetting)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
