import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystmSettingListComponent } from './list/systmSetting-list.component';
import { SystmSettingViewComponent } from './view/systmSetting-view.component';

const routes: Routes = [
    { path: 'list', component: SystmSettingListComponent },
    { path: 'view/:id', component: SystmSettingViewComponent, data: { title: '报警温度'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SystmSettingRoutingModule { }
