import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { SystmSettingRoutingModule } from './systmSetting-routing.module';
import { SystmSettingListComponent } from './list/systmSetting-list.component';
import { SystmSettingViewComponent } from './view/systmSetting-view.component';
import { SystmSettingEditComponent } from './edit/systmSetting-edit.component';

const COMPONENTS = [SystmSettingListComponent,SystmSettingViewComponent];
const COMPONENTS_NOROUNT = [SystmSettingEditComponent];

@NgModule({
    imports: [SharedModule,SystmSettingRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class SystmSettingModule { }
