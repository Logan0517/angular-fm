import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { SystmSettingViewComponent } from '../view/systmSetting-view.component';
import { SystmSettingEditComponent } from '../edit/systmSetting-edit.component';
import { SystmSettingService } from '../systmSetting.service';
import { SystmSetting } from '../systmSetting';
import { SystmSettingVO } from '../systmSetting.vo';

@Component({
    selector: 'app-systmsetting-list',
    templateUrl: './systmsetting-list.component.html',
    styles: [],
    providers: [SystmSettingEditComponent,SystmSettingViewComponent],
})
export class SystmSettingListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private systmSettingService: SystmSettingService,
  ) {}

  systmSetting: SystmSetting;
  systmSettingVO: SystmSettingVO;
  systmSettingVOs: SystmSettingVO[];
  systmSettings: SystmSetting[];
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '', index: 'id' },
    { title: '', index: 'isDel' },
    { title: '报警温度', index: 'temperatureWarnning' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: SystmSetting) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: SystmSetting) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: SystmSetting) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.systmSettingVO = new SystmSettingVO();
    this.systmSettingVO.page = 1;
    this.systmSettingVO.pageSize = 15;
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
    });
  }

  search() {
    this.loading = true;
    this.systmSettingService
      .getByPage(this.systmSettingVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.systmSettings = res.datas;
        this.systmSettingVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.systmSettingVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: SystmSetting) {
    this.modal.createStatic(SystmSettingEditComponent, { systmSettingVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: SystmSetting) {
    this.loading = true;
    this.systmSettingService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: SystmSetting) {
    this.router.navigate(['/systmSetting/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.systmSettingVOs = [];
    this.selectedRows.forEach(selectRow => {
      const systmSettingTmpVO: SystmSettingVO = new SystmSettingVO();
      systmSettingTmpVO.id = selectRow.id;
      this.systmSettingVOs.push(systmSettingTmpVO);
    });
    this.systmSettingService
    .delByVOForLogicMul(this.systmSettingVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.systmSettingVOs = [];
    this.selectedRows.forEach(selectRow => {
    const systmSettingTmpVO: SystmSettingVO = new SystmSettingVO();
      systmSettingTmpVO.id = selectRow.id;
    this.systmSettingVOs.push(systmSettingTmpVO);
    });
    this.systmSettingService
    .updMul(this.systmSettingVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
