
/*
* Created by jiangchen
* Automated Build
* 实体 SystmSetting 报警温度
*/
export class SystmSettingVO {
    id: any;/**/
    isDel: any;/**/
    temperatureWarnning: any;/*报警温度*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}