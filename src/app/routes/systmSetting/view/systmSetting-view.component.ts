import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { SystmSettingService } from '../systmSetting.service';
import { SystmSetting } from '../systmSetting';
import { SystmSettingVO } from '../systmSetting.vo';


@Component({
    selector: 'app-systmsetting-view',
    templateUrl: './systmsetting-view.component.html',
})
export class SystmSettingViewComponent implements OnInit {

    loading = false;
    systmSetting: SystmSetting = new SystmSetting();
    systmSettingVO: SystmSettingVO = new SystmSettingVO();


    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private systmSettingService:SystmSettingService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: SystmSetting) => {
            this.systmSettingVO.id = param.id;
        });
        this.loading = true;
        this.systmSettingService
        .getByVo(this.systmSettingVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: SystmSetting) => {
            this.systmSetting = res;
        });
    }


}
