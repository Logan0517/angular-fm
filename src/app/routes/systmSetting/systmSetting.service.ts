import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class SystmSettingService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'systmSetting/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'systmSetting/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'systmSetting/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'systmSetting/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'systmSetting/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'systmSetting/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'systmSetting/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'systmSetting/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'systmSetting/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'systmSetting/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'systmSetting/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'systmSetting/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'systmSetting/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'systmSetting/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'systmSetting/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'systmSetting/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'systmSetting/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'systmSetting/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'systmSetting/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'systmSetting/getForRelation';
        return this.http.post(url,vo);
    }
}
