import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SimpleGuard } from '@delon/auth';
import { environment } from '@env/environment';

import { LayoutDefaultComponent } from '../layout/default/default.component';
import { LayoutFullScreenComponent } from '../layout/fullscreen/fullscreen.component';
import { LayoutPassportComponent } from '../layout/passport/passport.component';
import { UserLoginComponent } from './passport/login/login.component';
import { UserRegisterComponent } from './passport/register/register.component';
import { UserRegisterResultComponent } from './passport/register-result/register-result.component';
// single pages
import { CallbackComponent } from './callback/callback.component';
import { UserLockComponent } from './passport/lock/lock.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutDefaultComponent,
    canActivate: [SimpleGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'exception', loadChildren: () => import('./exception/exception.module').then(m => m.ExceptionModule)},
      {
        path: 'area', // 区域
        loadChildren: () => import('./area/area.module').then(m => m.AreaModule),
      },
      {
        path: 'groupHasAccount', // 用户分组多对多
        loadChildren: () => import('./groupHasAccount/groupHasAccount.module').then(m => m.GroupHasAccountModule),
      },
      {
        path: 'role', // 角色
        loadChildren: () => import('./role/role.module').then(m => m.RoleModule),
      },
      {
        path: 'probeGroup', // 探头分组
        loadChildren: () => import('./probeGroup/probeGroup.module').then(m => m.ProbeGroupModule),
      },
      {
        path: 'powerHasGroup', // 权限分组多对多
        loadChildren: () => import('./powerHasGroup/powerHasGroup.module').then(m => m.PowerHasGroupModule),
      },
      {
        path: 'probe', // 探头
        loadChildren: () => import('./probe/probe.module').then(m => m.ProbeModule),
      },
      {
        path: 'warnningData', // 报警数据
        loadChildren: () => import('./warnningData/warnningData.module').then(m => m.WarnningDataModule),
      },
      {
        path: 'accountHasArea', // 
        loadChildren: () => import('./accountHasArea/accountHasArea.module').then(m => m.AccountHasAreaModule),
      },
      {
        path: 'supplier', // 供应商
        loadChildren: () => import('./supplier/supplier.module').then(m => m.SupplierModule),
      },
      {
        path: 'temperature', // 温度数据
        loadChildren: () => import('./temperature/temperature.module').then(m => m.TemperatureModule),
      },
      {
        path: 'position', // 职务
        loadChildren: () => import('./position/position.module').then(m => m.PositionModule),
      },
      {
        path: 'power', // 权限
        loadChildren: () => import('./power/power.module').then(m => m.PowerModule),
      },
      {
        path: 'device', // 设备
        loadChildren: () => import('./device/device.module').then(m => m.DeviceModule),
      },
      {
        path: 'manyTable', // 测试多对多表
        loadChildren: () => import('./manyTable/manyTable.module').then(m => m.ManyTableModule),
      },
      {
        path: 'account', // 用户账号
        loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
      },
      {
        path: 'group', // 用户分组
        loadChildren: () => import('./group/group.module').then(m => m.GroupModule),
      },
      {
        path: 'systmSetting', // 报警温度
        loadChildren: () => import('./systmSetting/systmSetting.module').then(m => m.SystmSettingModule),
      },
    ],
  },
  // passport
  {
    path: 'passport',
    component: LayoutPassportComponent,
    children: [
      { path: 'login', component: UserLoginComponent, data: { title: '登录', titleI18n: 'pro-login' } },
      { path: 'register', component: UserRegisterComponent, data: { title: '注册', titleI18n: 'pro-register' } },
      {
        path: 'register-result',
        component: UserRegisterResultComponent,
        data: { title: '注册结果', titleI18n: 'pro-register-result' },
      },
      { path: 'lock', component: UserLockComponent, data: { title: '锁屏', titleI18n: 'lock' } },
    ],
  },
  // 单页不包裹Layout
  { path: 'callback/:type', component: CallbackComponent },
  { path: '**', redirectTo: 'exception/404' },
];

  @NgModule({
    imports: [
      RouterModule.forRoot(routes, {
        useHash: environment.useHash,
        scrollPositionRestoration: 'top',
      }),
    ],
    exports: [RouterModule],
  })
  export class RouteRoutingModule {}
