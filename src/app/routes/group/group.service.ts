import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class GroupService {

    constructor(private http: HttpClient) { }

    getByVo(vo){
        const url = 'group/get';
        return this.http.post(url,vo);
    }

    add(entity){
        const url = 'group/add';
        return this.http.post(url,entity);
    }

    addMul(entity){
        const url = 'group/addMul';
        return this.http.post(url,entity);
    }

    delByIdentify(entity){
        const url = 'group/delByIdentify';
        return this.http.post(url,entity);
    }

    delByIdentifyMul(entity){
        const url = 'group/delByIdentifyMul';
        return this.http.post(url,entity);
    }

    delForLogicByIdentify(entity){
        const url = 'group/delForLogicByIdentify';
        return this.http.post(url,entity);
    }

    delForLogicByIdentifyMul(entity){
        const url = 'group/delForLogicByIdentifyMul';
        return this.http.post(url,entity);
    }

    delByVO(vo){
        const url = 'group/delByVO';
        return this.http.post(url,vo);
    }

    delByVOMul(vo){
        const url = 'group/delByVOMul';
        return this.http.post(url,vo);
    }

    delByVOForLogic(vo){
        const url = 'group/delByVOForLogic';
        return this.http.post(url,vo);
    }

    delByVOForLogicMul(vo){
        const url = 'group/delByVOForLogicMul';
        return this.http.post(url,vo);
    }

    upd(entity){
        const url = 'group/upd';
        return this.http.post(url,entity);
    }

    updMul(entity){
        const url = 'group/updMul';
        return this.http.post(url,entity);
    }

    updAndSetNull(entity){
        const url = 'group/updAndSetNull';
        return this.http.post(url,entity);
    }

    updAndSetNullMul(entity){
        const url = 'group/updAndSetNullMul';
        return this.http.post(url,entity);
    }

    getByPage(vo){
        const url = 'group/getByPage';
        return this.http.post(url,vo);
    }

    getCount(vo){
        const url = 'group/getCount';
        return this.http.post(url,vo);
    }

    getAllByVO(vo){
        const url = 'group/getAllByVO';
        return this.http.post(url,vo);
    }

    get(vo){
        const url = 'group/get';
        return this.http.post(url,vo);
    }

    getForRelation(vo){
        const url = 'group/getForRelation';
        return this.http.post(url,vo);
    }
}
