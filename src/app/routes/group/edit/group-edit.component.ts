import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NzMessageService, NzModalRef } from 'ng-zorro-antd';
import { GroupService } from '../group.service';
import { Group } from '../group';
import { GroupVO } from '../group.vo';
import { RoleService } from '../../role/role.service';
import { Role } from '../../role/role';
import { RoleVO } from '../../role/role.vo';
import { PositionService } from '../../position/position.service';
import { Position } from '../../position/position';
import { PositionVO } from '../../position/position.vo';

@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
})
export class GroupEditComponent implements OnInit {

  constructor(
    private msgSrv: NzMessageService,
    private modal: NzModalRef,
    private groupService: GroupService,
    private roleService: RoleService,
    private positionService: PositionService,
  ) {}

  form: FormGroup;
  loading = false;
  group: Group = new Group();
  groupVO: GroupVO = new GroupVO();
  roles: Role[];
  roleVO: RoleVO;
  positions: Position[];
  positionVO: PositionVO;

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.group = new Group();
    if(!this.groupVO){
        this.groupVO = new GroupVO();
    }
    if(this.groupVO.id){
      this.loading = false;
      this.groupService
        .getByVo(this.groupVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: any) => {
          this.group = res;
        });
    }
    this.roleVO = new RoleVO();
    this.roleService.getAllByVO(this.roleVO).subscribe((res: Role[]) => {
      this.roles = res;
    });
    this.positionVO = new PositionVO();
    this.positionService.getAllByVO(this.positionVO).subscribe((res: Position[]) => {
      this.positions = res;
    });
  }

  initForm() {
    this.form = new FormGroup({
        role: new FormControl({ name: 'role', value: '' }, Validators.required),/*角色*/
        position: new FormControl({ name: 'position', value: '' }, Validators.required),/*职位*/
        name: new FormControl({ name: 'name', value: '' }, [Validators.required]),/*组名*/
        isDel: new FormControl({ name: 'isDel', value: '' }, [Validators.required]),/*逻辑删除*/
        roleId: new FormControl({ name: 'roleId', value: '' }, [Validators.required]),/*角色*/
        positionId: new FormControl({ name: 'positionId', value: '' }, [Validators.required]),/*职位*/
    });
  }
    submit() {
        if (this.group.id) {
            this.groupService
                .upd(this.group)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
            });
            return;
        }
        this.groupService
            .add(this.group)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(() => {
                this.msgSrv.success(`操作成功`);
                this.modal.close('success');
        });
    }

    close(e) {
        this.modal.destroy();
    }
  }
