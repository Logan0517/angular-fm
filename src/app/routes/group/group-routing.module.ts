import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupListComponent } from './list/group-list.component';
import { GroupViewComponent } from './view/group-view.component';

const routes: Routes = [
    { path: 'list', component: GroupListComponent },
    { path: 'view/:id', component: GroupViewComponent, data: { title: '用户分组'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GroupRoutingModule { }
