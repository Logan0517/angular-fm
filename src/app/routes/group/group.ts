import { Role } from '../role/role';
import { Position } from '../position/position';
import { PowerHasGroup } from '../powerHasGroup/powerHasGroup';
import { GroupHasAccount } from '../groupHasAccount/groupHasAccount';

/*
* Created by jiangchen
* Automated Build
* 实体 Group 用户分组
*/
export class Group {
    id: any;/*标识*/
    name: any;/*组名*/
    isDel: any;/*逻辑删除*/
    roleId: any;/*角色*/
    positionId: any;/*职位*/
    role: Role;/*角色*/
    position: Position;/*职位*/
    groupPowerHasGroups: PowerHasGroup[];/* 权限分组多对多*/
    groupGroupHasAccounts: GroupHasAccount[];/* 用户分组多对多*/
}