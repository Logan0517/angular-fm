import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

import { ModalHelper } from '@delon/theme';
import { STComponent, STColumn, STData, STChange, STPage } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';

import { GroupViewComponent } from '../view/group-view.component';
import { GroupEditComponent } from '../edit/group-edit.component';
import { GroupService } from '../group.service';
import { Group } from '../group';
import { GroupVO } from '../group.vo';
import { RoleService } from '../../role/role.service';
import { Role } from '../../role/role';
import { RoleVO } from '../../role/role.vo';
import { PositionService } from '../../position/position.service';
import { Position } from '../../position/position';
import { PositionVO } from '../../position/position.vo';

@Component({
    selector: 'app-group-list',
    templateUrl: './group-list.component.html',
    styles: [],
    providers: [GroupEditComponent,GroupViewComponent],
})
export class GroupListComponent implements OnInit {

  constructor(
    public msg: NzMessageService,
    private modal: ModalHelper,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private groupService: GroupService,
    private roleService: RoleService,
    private positionService: PositionService,
  ) {}

  group: Group;
  groupVO: GroupVO;
  groupVOs: GroupVO[];
  groups: Group[];
  roles: Role[];
  roleVO: RoleVO;
  positions: Position[];
  positionVO: PositionVO;
  selectedRows: STData[] = [];
  loading = false;
  searchForm: FormGroup;

  @ViewChild('st', { static: true })
    st: STComponent;
    page: STPage = {
    front: false,
    total: true,
  };
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '标识', index: 'id' },
    { title: '组名', index: 'name' },
    { title: '逻辑删除', index: 'isDel' },
    { title: '角色', index: 'roleId' },
    { title: '职位', index: 'positionId' },
    {
      title: '操作',
      buttons: [
        {
          text: '编辑',
          click: (item: Group) => this.edit(item),
        },
        {
          text: '删除',
          type: 'del',
          click: (item: Group) => this.remove(item),
        },
        {
          text: '查看',
          click: (item: Group) => this.showInfo(item),
        },
      ],
    },
  ];

  ngOnInit() {
    this.initData();
    this.initSearchForm();
  }

  initData() {
    this.groupVO = new GroupVO();
    this.groupVO.page = 1;
    this.groupVO.pageSize = 15;
    this.roleVO = new RoleVO();
    this.roleService.getAllByVO(this.roleVO).subscribe((res: Role[]) => {
      this.roles = res;
    });
    this.positionVO = new PositionVO();
    this.positionService.getAllByVO(this.positionVO).subscribe((res: Position[]) => {
      this.positions = res;
    });
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      nameLike: new FormControl({ name: 'nameLike', value: '' }),/*组名模糊查询*/
      role: new FormControl({ name: 'role', value: '' }),/*角色*/
      position: new FormControl({ name: 'position', value: '' }),/*职位*/
    });
  }

  search() {
    this.loading = true;
    this.groupService
      .getByPage(this.groupVO)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.groups = res.datas;
        this.groupVO = res.vo;
      });
  }

  reset() {}

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        break;
      case 'filter':

        break;
      case 'pi':
        this.groupVO.page = e.pi;
        this.search();
        break;
    }
  }

  edit(param: Group) {
    this.modal.createStatic(GroupEditComponent, { groupVO: param }).subscribe(result => {
      if (result) {
        this.search();
      }
    });
  }

  remove(item: Group) {
    this.loading = true;
    this.groupService
    .delByVOForLogic(item)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  showInfo(item: Group) {
    this.router.navigate(['/group/view/'+ item.id]);
  }

  removeMul() {
    this.loading = true;
    this.groupVOs = [];
    this.selectedRows.forEach(selectRow => {
      const groupTmpVO: GroupVO = new GroupVO();
      groupTmpVO.id = selectRow.id;
      this.groupVOs.push(groupTmpVO);
    });
    this.groupService
    .delByVOForLogicMul(this.groupVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
      this.search();
    });
  }

  approval() {
    this.loading = true;
    this.groupVOs = [];
    this.selectedRows.forEach(selectRow => {
    const groupTmpVO: GroupVO = new GroupVO();
      groupTmpVO.id = selectRow.id;
    this.groupVOs.push(groupTmpVO);
    });
    this.groupService
    .updMul(this.groupVOs)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((res: any) => {
    this.search();
    });
  }

  export() {}
}
