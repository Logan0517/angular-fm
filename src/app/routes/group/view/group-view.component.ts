import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzTabChangeEvent } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { Router,  ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { GroupService } from '../group.service';
import { Group } from '../group';
import { GroupVO } from '../group.vo';
import { RoleService } from '../../role/role.service';
import { Role } from '../../role/role';
import { RoleVO } from '../../role/role.vo';
import { PositionService } from '../../position/position.service';
import { Position } from '../../position/position';
import { PositionVO } from '../../position/position.vo';

import { PowerHasGroupService } from '../../powerHasGroup/powerHasGroup.service';
import { PowerHasGroup } from '../../powerHasGroup/powerHasGroup';
import { PowerHasGroupVO } from '../../powerHasGroup/powerHasGroup.vo';
import { GroupHasAccountService } from '../../groupHasAccount/groupHasAccount.service';
import { GroupHasAccount } from '../../groupHasAccount/groupHasAccount';
import { GroupHasAccountVO } from '../../groupHasAccount/groupHasAccount.vo';

@Component({
    selector: 'app-group-view',
    templateUrl: './group-view.component.html',
})
export class GroupViewComponent implements OnInit {

    loading = false;
    group: Group = new Group();
    groupVO: GroupVO = new GroupVO();
    /** 权限分组多对多 */
    groupPowerHasGroups: PowerHasGroup[] = [];
    groupPowerHasGroupsVO: PowerHasGroupVO = new PowerHasGroupVO();
    groupPowerHasGroupsLoading = false;
    /** 用户分组多对多 */
    groupGroupHasAccounts: GroupHasAccount[] = [];
    groupGroupHasAccountsVO: GroupHasAccountVO = new GroupHasAccountVO();
    groupGroupHasAccountsLoading = false;

    /** 权限分组多对多 */
    groupPowerHasGroupsColumns: STColumn[] = [
        { title: '逻辑删除', index: 'isDel', default: '-'},
        { title: '分组', index: 'powerId', default: '-'},
        { title: '用户', index: 'groupId', default: '-'},
    ];
    /** 用户分组多对多 */
    groupGroupHasAccountsColumns: STColumn[] = [
        { title: '分组', index: 'groupId', default: '-'},
        { title: '用户', index: 'accountId', default: '-'},
    ];

    constructor(
        public msg: NzMessageService,
        private cdr: ChangeDetectorRef,
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private groupService:GroupService,
        private powerHasGroupService:PowerHasGroupService,
        private groupHasAccountService:GroupHasAccountService,
    ) {}

    ngOnInit() {
        this.initData();
        this.initGroupPowerHasGroups();
        this.initGroupGroupHasAccounts();
    }

    initData() {
        this.activatedRoute.params.subscribe((param: Group) => {
            this.groupVO.id = param.id;
        });
        this.loading = true;
        this.groupService
        .getByVo(this.groupVO)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe((res: Group) => {
            this.group = res;
        });
    }

    /** 权限分组多对多 */
    initGroupPowerHasGroups() {
        this.groupPowerHasGroupsLoading = true;
        this.groupPowerHasGroupsVO.groupVO= this.groupVO;
        this.powerHasGroupService
        .getAllByVO(this.groupPowerHasGroupsVO)
        .pipe(finalize(() => (this.groupPowerHasGroupsLoading = false)))
        .subscribe((res: PowerHasGroup[]) => {
            this.groupPowerHasGroups = res;
        });
    }

    /** 用户分组多对多 */
    initGroupGroupHasAccounts() {
        this.groupGroupHasAccountsLoading = true;
        this.groupGroupHasAccountsVO.groupVO= this.groupVO;
        this.groupHasAccountService
        .getAllByVO(this.groupGroupHasAccountsVO)
        .pipe(finalize(() => (this.groupGroupHasAccountsLoading = false)))
        .subscribe((res: GroupHasAccount[]) => {
            this.groupGroupHasAccounts = res;
        });
    }


}
