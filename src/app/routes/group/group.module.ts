import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { GroupRoutingModule } from './group-routing.module';
import { GroupListComponent } from './list/group-list.component';
import { GroupViewComponent } from './view/group-view.component';
import { GroupEditComponent } from './edit/group-edit.component';

const COMPONENTS = [GroupListComponent,GroupViewComponent];
const COMPONENTS_NOROUNT = [GroupEditComponent];

@NgModule({
    imports: [SharedModule,GroupRoutingModule],
    declarations: [...COMPONENTS,...COMPONENTS_NOROUNT],
    entryComponents: COMPONENTS_NOROUNT
})
export class GroupModule { }
