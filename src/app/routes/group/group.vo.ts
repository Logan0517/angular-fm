import { RoleVO } from '../role/role.vo';
import { PositionVO } from '../position/position.vo';
import { PowerHasGroupVO } from '../powerHasGroup/powerHasGroup.vo';
import { GroupHasAccountVO } from '../groupHasAccount/groupHasAccount.vo';

/*
* Created by jiangchen
* Automated Build
* 实体 Group 用户分组
*/
export class GroupVO {
    id: any;/*标识*/
    name: any;/*组名*/
    nameLike: any;/*组名模糊查询*/
    isDel: any;/*逻辑删除*/
    roleId: any;/*角色*/
    positionId: any;/*职位*/
    roleVO: RoleVO;/*角色*/
    positionVO: PositionVO;/*职位*/
    groupPowerHasGroupsVO: PowerHasGroupVO[];/* 权限分组多对多*/
    groupGroupHasAccountsVO: GroupHasAccountVO[];/* 用户分组多对多*/
    totalCount: number;/*总记录数*/
    page: number;/*当前页码*/
    pageSize: number;/*每页记录数量*/
    startRecord: number;/*开始查询记录*/
}